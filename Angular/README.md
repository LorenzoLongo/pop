# Angular Course

This folder contains all the files concerning the Udemy Angular course. This includes applications, assignments and projects. Also a theoretical summary will be provided.

> LINK: https://www.udemy.com/the-complete-guide-to-angular-2/

## Angular Course Certificate

Certificate of completion of the Angular Course

![Certificate of completion Angular 7](certificate.jpg)

## Course Sections

### Section 1 (36:31)

Getting started with Angular 7

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%201

### Section 2 (01:34:32)

Angular Basics

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%202

### Section 3 (01:03:43)

Course Project - Angular Basics

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%203

### Section 4 (12:09)

Debugging in Angular

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%204

### Section 5 (01:11:55)

Components & Deep Dive Databinding

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%205

### Section 6 (31:07)

Course Project - Components & Deep Dive Databinding

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%206

### Section 7 (47:02)

Deep Dive Directives

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%207

### Section 8 (06:25)

Course Project - Directives

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%208

### Section 9 (34:14)

Using Services & Dependency Injection

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%209

### Section 10 (30:48)

Course Project - Services & Dependency Injection

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2010

### Section 11 (02:18:12)

Changing Pages with Routing

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2011

### Section 12 (45:32)

Course Project - Routing

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2012

### Section 13 (38:42)

Understanding Observables

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2013

### Section 14 (03:51)

Course Project - Obervables

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2014

### Section 15 (01:48:31)

Handling Forms in Angular Apps

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2015

### Section 16 (01:14:36)

Course Project - Forms

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2016

### Section 17 (37:03)

Using Pipes to Transform Output

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2017

### Section 18 (39:29)

Making Http Requests

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2018

### Section 19 (21:22)

Course Project - Http

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2019

### Section 20 (44:30)

Authentication & Route Protection in Angular Apps

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2020

### Section 21 (01:27:27)

Using Angular Modules & Optimizing Apps

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2021

### Section 22 (11:49)

Deploying an Angular App

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2022

### Section 23 (51:36)

Bonus: The HttpClient

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2023

### Section 24 (03:41:08)

Bonus: Working with NgRx in our Project

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2024

### Section 25 (27:08)

Bonus: Angular Universal

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2025

### Section 26 (39:22)

Angular Animations

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2026

### Section 27 (27:23)

Adding Offline Capabilities with Service Workes

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2027

### Section 28 (45:19)

A Basic Introduction to Unit Testing in Angular Apps

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2028

### Section 29 (35:53)

Angular Changes & New Features

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2029

### Section 30 (01:59)

Course Roundup

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2030

### Section 31 (51:02)

Custom Project & Workflow Setup

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2031

### Section 32 (25:55)

Bonus: TypeScript Introduction (for Angular 2 Usage)

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2032  

