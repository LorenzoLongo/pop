# Getting started with Angular 7

This folder contains all the information of section 1 of the Angular 7 Course.

## Logbook Section 1

    Lorenzo Longo - 04/03/2019
        > Watching course videos 1-6
        > Create first Angular application
        > Try to upload to GIT -> (couldn't push from Webstorm)
         
    Lorenzo Longo - 07/03/2019
        > Watching course 1-6 (second/third attempt)
        > Create first Angular application (second/third attempt)
        > Upload to GIT
        > Watching Course Videos 7-11
        
    Lorenzo Longo - 09/03/2019
        > Writing Theory Section 1
        
        
## Links

>Push to GIT: https://www.jetbrains.com/help/webstorm/commit-and-push-changes.html
 
>Angular File Structure: https://angular.io/guide/file-structure

>Angular Theory: https://angular.io/