# Applications of Section 10

Here you can find all of the applications that are made inside section 10 of the Angular Course.

## Summary


### Course Project - Recipe book and shopping list application

    Adding services to the project
    
#### Application Structure

    Below you can find the structure of the application
    
![Application structure](./S10_Application_Structure.jpg)

#### Application Result

    Below you can find the result of the course project application

![Application result](./S10_Result_Course_Project.mp4)