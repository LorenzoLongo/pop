# Course Project - Services & Dependency Injection

This folder contains all the information of section 10 of the Angular 7 Course.

## Logbook Section 10
        
    Lorenzo Longo - 02/04/2019
        > Create Section 10 Application
            -> Create Recipe Service
            -> Cross-Component Communication Service
            -> Shopping List Service
            -> Service to Push Data
            -> Passing Data Between Components
        > Watch Course Videos 114-121
        > Writing Theory Section 10
        
    Lorenzo Longo - 03/04/2019
        > Update Section 10 Application