# Applications of Section 11

Here you can find all of the applications that are made inside section 11 of the Angular Course.

## Summary

### Changing Pages with Routing

    Why use Router?
    Setting up and loading routes
    Navigating with Router Links
    Navigation Paths
    Styling Active Router Links
    Navigating Programmatically
    Relative Paths in Programmatic Navigation
    Passing Parameters to Routes
    Fetching Router Parameters (Reactively)
    Passing Query Parameters & Fragments
    Retrieving Query Parameters & Fragments
    Setting up Child Nested Routes
    Using Query Parameters
    Handling Query Parameters
    Redirecting & Wildcard Routes
    Outsourcing the Route Configuration
    CanActive Guard to protect Routes
    CanActiveChild Guard to protect Child Routes
    Fake Authentication Service (simulation)
    CanDeactivate to control Navigation
    Passing Static Data to a Route
    Resolve Guard to resolve Dynamic Data
    Location Strategies
    