# Routing

This folder contains all the information of section 11 of the Angular 7 Course.

## Logbook Section 11

    Lorenzo Longo - 05/04/2019
        > Create Section 11 Application
            -> Setting Up Routes
            -> Loading Routes
            -> Router Link Navigation
            -> Styling Active Router Links
            -> Programmatically Navigation
            -> Passing Parameters to Routes
            -> Fetching Route Parameters
            -> Passing/Retrieving Query Parameters & Fragments
            -> Setup Child Nested Routes
        > Watch Course Videos 122-138
        > Writing Theory Section 11
        
    Lorenzo Longo - 06/04/2019
        > Update Section 11 Application
            -> Configuring the Handling of Query Parameters
            -> Redirecting
            -> Wildcard Routes
            -> canActivate Guard
            -> canActivateChild Guard (Nested Routes)
            -> Fake Auth Service
            -> canDeactive (Control Navigation)
            -> Pass Static Data to Route
            -> Resolve Dynamic Data (Resolve Guard)
        > Watch Course Videos 139-152
        > Writing Theory Section 11
        
## Links

> Angular CanLoad: https://angular.io/api/router/CanLoad

> Angular CanDeactivate: https://angular.io/api/router/CanDeactivate

> Angular Routing: https://angular.io/tutorial/toh-pt5

> Angular Router: https://angular.io/guide/router