# Course Project - Routing

This folder contains all the information of section 12 of the Angular 7 Course.

## Logbook Section 12

    Lorenzo Longo - 07/04/2019
        > Create Section 12 Application
            -> Setting Up Routes
            -> Mark Active Route
            -> Adding Child Routes
            -> Configure Route Parameters
            -> Programmatic Navigation
        > Watch Course Videos 153-167
        > Writing Theory Section 12

