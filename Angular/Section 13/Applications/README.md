# Applications of Section 13

Here you can find all of the applications that are made inside section 13 of the Angular Course.

## Summary

### Understanding Observables

    Analyzing a Built-in Angular Observable
    Building a Simple Observable
    Using a Simple Observable
    Building a Custom Observable from Scratch
    Using a Custom Observable
    Unsubscribe
    Using Subjects to Pass and Liston to Data
    Observable Operators
    RxJS without rxjs-compat package