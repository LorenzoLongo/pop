# Understanding Observables

This folder contains all the information of section 13 of the Angular 7 Course.

## Logbook Section 13

    Lorenzo Longo - 08/04/2019
        > Create Section 13 Application
            -> Angular Observables
            -> Custom Observable
            -> Errors & Completion
            -> Operators
            -> Subjects
        > Watch Course Videos 168-177
        > Writing Theory Section 13

## Links

> Angular Observables: https://angular.io/guide/observables