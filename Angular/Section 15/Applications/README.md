# Applications of Section 15

Here you can find all of the applications that are made inside section 15 of the Angular Course.

## Summary

    Template Driven:
        Creating a Form and Registering Controls
        Submitting and Using the Form
        Accessing the Form with @ViewChild
        Adding Validation to check User Input
        Built-in Validators
        HTML5 Validation
        Using the Form State
        Output a Validation Error Message
        Set Default Values with ngModel Property Binding
        Two-Way-Binding
        Grouping Form Controls
        Handling Radio Buttons
        Setting and Patching Form Values
        Using Form Data
        Resetting Forms
    
    Reactive Forms:
        Creating a Form in Code
        Syncing HTML and Form code
        Submitting a Form
        Adding Validation
        Getting Access to Controls
        Grouping Controls
        Arrays of Form Controls (FormArray)
        Create Custom Validators
        Using Error Codes
        Creating Custom Async Validator
        Reacting to Status or Value Changes
        Setting and Patching default Values