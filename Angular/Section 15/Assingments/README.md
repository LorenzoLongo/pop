# Assignments of Section 15

Here you can find all of the assigments that are made inside section 15 of the Angular Course.

## Summary

### Assignment 1 - Template Driven Forms

 > LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2015/Assingments/S15A1
    
    > 1. Add a form with the following inputs and validators:
            1.1) Mail address (with validator, required)
            1.2) Dropdown with three different Subscriptions ('Basic', 'Advanced', 'Pro')
            1.3) Set the 'Advanced' option as the default 
            1.4) Password field (required)
            1.5) Submit button
    > 2. Display a warning message if the form is invalid (the form must be touched)
    > 3. Display a warning message below every input that is invalid (the input must be toudhed)
    > 4. Print the values of the form into a console.log()
    
    
![end result of assignment 1](./S15A1_result.mp4)

### Assignment 2 - Reactive Forms

 > LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2015/Assingments/S15A2
 
    < 1. Add a form with the following controls and validators:
            1.1) Project Name (with validator, required)
            1.2) Mail (with validators, required and email)
            1.3) Project Status Dropdown (values: 'Stable', 'Critical', 'Finished')
            1.4) Submit Button
    < 2. Create a validator that doesn't allow "Test" as a Project Name
    < 3. Create a async validator for the Project Name
    < 4. Print the Form Values into the console
    