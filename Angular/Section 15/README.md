# Handling Forms in Angular Apps

This folder contains all the information of section 15 of the Angular 7 Course.

## Logbook Section 15

    Lorenzo Longo - 10/04/2019
        > Create Section 15 Application
            TEMPLATE DRIVEN:
                -> Template Driven vs Reactive Approach
                -> Creating Form + Controls
                -> Submitting & Using Form
                -> Accessing the Form with @ViewChild
                -> Validation (User Input)
                -> Built-In Validators
                -> Validation Error Messages
                -> ngModel Property Binding
                -> ngModel Two-Way Binding
                -> Grouping Form Controls
                -> Handling Radio Buttons
                -> Setting & Patching Form Values
                -> Using Form Data
                -> Resetting Forms
        > Watch Course Videos 180-198
        > Writing Theory Section 15
        
    Lorenzo Longo - 11/04/2019
        > Creating Assignment 1 of Section 15
        
    Lorenzo Longo - 13/04/2019
        > Update Assignment 1 of Section 15
        
    Lorenzo Longo - 14/04/2019
        > Update Section 15 Application
            REACTIVE:
                -> Programmatically Create Form
                -> Syncing HTML & Form
                -> Submitting the Form
                -> Adding Validation
                -> Getting Access to Controls
                -> Grouping Controls
                -> Arrays of Form Controls
                -> Custom Validators
                -> Error Codes
                -> Async Validator
                -> Reacting to Status/Value Changes
                -> Setting & Patching Values
        > Watch Course Videos 199-213
        > Writing Theory Section 15
        
    Lorenzo Longo - 15/04/2019
        > Creating Assignment 2 of Section 15
        
## Links

> Angular Validators: https://angular.io/api/forms/Validators

> Angular Template Driven Forms: https://angular.io/guide/forms

> Angular Reactive Forms: https://angular.io/guide/reactive-forms