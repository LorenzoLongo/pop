# Applications of Section 16

Here you can find all of the applications that are made inside section 16 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application
    
    Adding routes to the project
    
#### Application Result
    
    Below you can find the result of the course project application
    
![Application result](./S16_Result_Course_Project.mp4)