# Course Project - Forms

This folder contains all the information of section 16 of the Angular 7 Course.

## Logbook Section 16

    Lorenzo Longo - 16/04/2019
        > Create Section 16 Application
            -> TD: Shopping List Form
            -> Validation
            -> Loading Shopping List Items in Form
            -> Updating Existing Items
            -> Resetting Form
            -> Cancel Form (User)
            -> Delete Shopping List Items
            -> Editing Recipes Form
            -> Syncing HTML with Form
            -> Adding Controls to Form Array
            -> Validating User Input
            -> Submitting the Recipe Edit Form
            -> Image Preview
            -> Deleting Ingredients
            -> Deleting all Items in a Form Array
        > Watch Course Videos 214-236

