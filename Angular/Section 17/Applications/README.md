# Applications of Section 17

Here you can find all of the applications that are made inside section 17 of the Angular Course.

## Summary

    Using Pipes
    Parameterizing Pipes
    Chaining Multiple Pipes
    Creating a Custom Pipe
    Parameterizing a Custom Pipe
    Creating a Filter Pipe 
    Pure and Impure Pipes (live rendering on updating a object or array)
    Async Pipes
    
![end result of application 1](S17_Course_Result.mp4)