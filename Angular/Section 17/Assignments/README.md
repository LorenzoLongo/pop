# Assignments of Section 17

Here you can find all of the assigments that are made inside section 17 of the Angular Course.

## Summary

### Assignment 1 - Pipes

 > LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2017/Assingments/S17A1
    
    Create a reverse word pipe
    Create a sort servers pipe
    
    
![end result of assignment 1](./S17A1_result.mp4)