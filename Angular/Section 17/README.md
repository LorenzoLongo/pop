# Using Pipes to Transform Output

This folder contains all the information of section 17 of the Angular 7 Course.

## Logbook Section 17

    Lorenzo Longo - 16/04/2019
        > Create Section 17 Application
            -> Using Pipes
            -> Parameterizing Pipes
            -> Chaining Pipes
            -> Custom Pipes
            -> Parameterizing Custom Pipes
            -> Pure/Impure Pipes
            -> Async Pipes
        > Watch Course Videos 237-246
        > Writing Theory Section 17

    Lorenzo Longo - 17/04/2019
        > Create Assignment 1 of Section 17
        > Watch Course Videos 180-198
        
## Links

> Angular Pipes: https://angular.io/api?query=pipe