# Applications of Section 18

Here you can find all of the applications that are made inside section 18 of the Angular Course.

## Summary

    Firebase Setup
    Sending POST Requests
    Adjusting Request Headers
    Sending GET Requests
    Sending a PUT Request
    Transform Responses with Observable Operators (map())
    Using Returned Data
    Catching Http Errors
    Using Async Pipe with Http Requests
    
    
![end result of application 1](S18_Course_Result.mp4)