# Making Http Requests

This folder contains all the information of section 18 of the Angular 7 Course.

## Logbook Section 18

    Lorenzo Longo - 18/04/2019
        > Create Section 18 Application
            -> Firebase Setup
            -> Sending a Post Request
            -> GET Data
            -> RxJS Operators
            -> Types with HTTPClient
            -> Outputting Posts
            -> Service for HTTP Requests
            -> Delete Request
            -> Handling Errors
            -> Subjects for Error Handling
            -> catchError Operator
            -> Setting Headers
            -> Query Params
            -> Observing Responses
            -> Change Response Body Type
            -> Manipulating Request Objects
            -> Interceptors
        > Watch Course Videos 247-274
        > Writing Theory Section 18
        
## Links

> Firebase: https://firebase.google.com/

> Firebase Demo: https://ng-ivy-demo.firebaseapp.com/

> Angular Http: https://angular.io/guide/http