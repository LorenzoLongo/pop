# Applications of Section 19

Here you can find all of the applications that are made inside section 19 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application
        
    Adding Http requests to the application
        
        PUT request (save recipes)
        GET request (fetch recipes)
        
#### Application Result
        
    Below you can find the result of the course project application
        
![Application result](./S19_Result_Course_Project.mp4)