# Course Project - Http

This folder contains all the information of section 19 of the Angular 7 Course.

## Logbook Section 19

    Lorenzo Longo - 18/04/2019
        > Create Section 19 Application
            -> Firebase Setup
            -> DataStorage Service
            -> Storing Recipes
            -> Fetching Recipes
            -> Transforming Response Data
            -> Resolve Data Before Loading
        > Watch Course Videos 275-282
        > Writing Theory Section 19