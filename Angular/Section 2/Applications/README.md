# Applications of Section 2

Here you can find all of the applications that are made inside section 2 of the Angular Course.

## Summary

### Basics 1

     Creating components manually
     Creating components dynamically
     Styling components (inline, external file, bootstrap)
     Component selector types (as elements, as classes)
     String Interpolation
     Property Binding
     Event Binding
     Bind properties with events
     Pass and use data with events
     Two-way-databinding
     Directives
        > ngIf/Else -> outputting data
        > ngStyle -> styling elements
        > ngClass -> applying CSS classes dynamically
        > ngFor -> outputting lists, indexing