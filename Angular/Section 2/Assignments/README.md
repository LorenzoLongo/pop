# Assignments of Section 2

Here you can find all of the assigments that are made inside section 2 of the Angular Course.

## Summary

### Assignment 1 - Components/Styling/Templates

 > LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%202/Assignments/S2A1
    
    > 1. Create two or more Components (manually and with CLI) - Warning and Success alert
    > 2. Output them beneath each other in the AppComponent
    > 3. Style the Components properly (red/green text messages)
    > 4. Use different styles (external or internal templates)
    > 5. Nest the components into each other (for practise)
    > 6. Use different type of selectors (elements, classes)
    
    
![end result of assignment 1](./S2A1_result.jpg)

### Assignment 2 - Two-Way-Binding/String Interpolation/Event Binding/Property Binding

 > LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%202/Assignments/S2A2
 
    > 1. Add an input field with property of 'username' via Two-Way-Binding
    > 2. Output the username property via String Interpolation (in a <p>)
    > 3. Add a button which only may be clicked if the username is NOT an empty string
    > 4. Clicking the button should reset the username to an empty string

![Assingment 2 result](./S2A2.mp4)

### Assignment 3 - Directives (ngIf, ngFor, ngClass, ngStyle)

 > LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%202/Assignments/S2A3
 
    > 1. Add a button with "Display Details" as text
    > 2. Add a paragraph with content of your choice
    > 3. Toggle the paragraph by clicking on the button
    > 4. Log all button clicks and display it below the paragraph (timestamp)
    > 5. At the 5th log item, all log items must have a blue background and white text color (ngStyle + ngClass)
    
 ![Assingment 3 result](./S2A3.mp4)

