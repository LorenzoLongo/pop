import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessCLIComponent } from './success-cli.component';

describe('SuccessCLIComponent', () => {
  let component: SuccessCLIComponent;
  let fixture: ComponentFixture<SuccessCLIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessCLIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessCLIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
