import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  username = '';
  enableBtn = false;

  constructor() {
    setTimeout(() => {
      if(this.username != '') {
        this.enableBtn = true;
      }
    }, 2000);
  }

  ngOnInit() {
  }

  onUpdateUsername(event: Event) {
    this.username = (<HTMLInputElement>event.target).value;
  }

  onBtnClick() {
    this.username = '';
    this.enableBtn = false;
  }
}
