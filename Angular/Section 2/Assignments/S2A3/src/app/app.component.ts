import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'S2A3';

  state = false;
  log = [];

  onToggleDetails() {
    this.state = !this.state;
    //this.log.push(this.log.length + 1);
    this.log.push(new Date());
  }
}
