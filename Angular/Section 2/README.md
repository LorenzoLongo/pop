# Angular Basics

This folder contains all the information of section 2 of the Angular 7 Course.

## Logbook Section 2

    Lorenzo Longo - 13/03/2019
        > Create Section 2 Application
        > Start Doing Assignment 1 of the Course Section 2
            -> Creating Components (Custom + CLI)
            -> App Module + Component Declaration
            -> Nesting Components
            -> Component Styles & Templates
            -> Component Selector
        > Watching Course Videos 12-21
        > Writing Theory Section 2
        
    Lorenzo Longo - 14/03/2019
        > Finishing Assignment 1 of Course Section 2
    
    Lorenzo Longo - 15/03/2019
        > Update Section 2 Application
        > Watching Course Videos 22-26
        > Writing Theory Section 2
    
    Lorenzo Longo - 16/03/2019
        > Update Section 2 Application
        > Watching Course Videos 27-32
        > Writing Theory Section 2
        
    Lorenzo Longo - 18/03/2019
        > Create Assignment 2
            -> Databinding
            -> String Interpolation
            -> Event Binding    
            -> Bindable Properties + Events
            -> Passing + Using Data with Eventbinding
            ->  Two-Way Databinding
    
    Lorenzo Longo - 19/03/2019
        > Update Section 2 Application
        > Create Assignment 3
            -> Directives
            -> Output Data (*ngIf / Else)
            -> ngStyle
            -> ngClass
            -> ngFor + (index)
        > Watch Course Videos 33-41
        > Writing Theory Section 2
        
## Links

> Testing Components: https://semaphoreci.com/community/tutorials/testing-components-in-angular-2-with-jasmine

> Directives: https://angular.io/api?type=directive