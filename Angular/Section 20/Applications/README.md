# Applications of Section 20

Here you can find all of the applications that are made inside section 20 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application
        
    Adding Http requests to the application
        
        Authentication in Single Page Applications
        Creating a Signup Page
        Creating a Signup Page Route
        Setting up the Firebase SDK
        Use getIdToken method
        Signing Users Up
        Signing Users In
        Requiring a Token
        Sending the Token
        Checking and Using Authentication Status
        Adding a Logout Button
        Route Protection
        Route Redirection
        
#### Application Result
        
    Below you can find the result of the course project application
        
![Application result](./S20_Result_Course_Project.mp4)
