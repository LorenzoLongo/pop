# Authentication & Route Protection in Angular Apps

This folder contains all the information of section 20 of the Angular 7 Course.

## Logbook Section 20

    Lorenzo Longo - 19/04/2019
        > Create Section 20 Application
            -> Authentication Modes
            -> Handling Form Input
            -> Preparing/Sending Signup Request
            -> Error Handling
            -> Sending Login Requests
            -> Creating/Storing User Data
            -> Adding Token to Requests
            -> Attaching Token with Intercepter
            -> Adding Logout
            -> Adding Auth Guard
        > Watch Course Videos 283-304
        > Writing Theory Section 20
     
    Lorenzo Longo - 20/04/2019
        > Update Section 20 Application

## Links

> Firebase Authentication: https://firebase.google.com/docs/reference/rest/auth

> JWT Token: https://jwt.io/introduction/

