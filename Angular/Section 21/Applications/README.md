# Applications of Section 21

Here you can find all of the applications that are made inside section 21 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application
        
    Restructure the application + add optimizations
        Creating a Feature Module for Recipes
        Importance of the order of the Imports 
        Registering Routes in a Feature Module
        Creating a Shared Module
        Creating a Shopping List Feature Module
        Creating the Auth Feature Module
        Adding Lazy Loading to the Recipes Module
        Protecting Lazy Loaded Routes with canLoad
        Creating a Basic Core Module
        Restruturing Services to use the Child Injector
        Ahead of Time Compilation in CLI
        Preloading Lazy Loaded Routes