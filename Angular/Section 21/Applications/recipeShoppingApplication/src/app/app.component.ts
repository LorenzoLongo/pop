import {Component, OnInit} from '@angular/core';
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'recipe';


  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyCQzkqqYGT7Pi5xPStaHOQW-layuX45Ol0",
      authDomain: "ng-recipe-book-4365b.firebaseapp.com",
      databaseURL: "https://ng-recipe-book-4365b.firebaseio.com",
      projectId: "ng-recipe-book-4365b",
      storageBucket: "ng-recipe-book-4365b.appspot.com",
      messagingSenderId: "233348888183"
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
