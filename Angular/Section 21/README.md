# Using Angular Modules & Optimizing Apps

This folder contains all the information of section 21 of the Angular 7 Course.

## Logbook Section 21

    Lorenzo Longo - 20/04/2019
        > Create Section 21 Application
            -> Analyzing AppModule
            -> Feature Modules
            -> Splitting Modules
            -> Add Routes to Feature Modules
            -> Component Declarations
            -> Shared Module
            -> Core Module 
            -> Auth Module
            -> Implementing Lazy Loading
            -> Preloading Lazy Loaded Code
            -> Loading Service Differently
            -> Ahead-of-Time Compilation
        > Watch Course Videos 305-325
        > Writing Theory Section 21
        
    Lorenzo Longo - 21/04/2019
        > Update Section 21 Application

## Links

> Angular NgModules: https://angular.io/guide/ngmodules

> NgModule FAQ: https://angular.io/guide/ngmodule-faq