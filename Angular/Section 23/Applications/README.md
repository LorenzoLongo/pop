# Applications of Section 23

Here you can find all of the applications that are made inside section 23 of the Angular Course.

## Summary

    Implementing the HttpClientModule
    Request Configuration and Response
    Requesting Events
    Setting Headers
    Http Parameters
    Progress Object
    Interceptors
    Modifying Requests
    Modifying Requests in Interceptors
    