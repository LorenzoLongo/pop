import {Component} from '@angular/core';
import {DataStorageService} from "../../shared/data-storage.service";
import {AuthService} from "../../auth/auth.service";
import {HttpEvent, HttpEventType} from "@angular/common/http";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})

export class HeaderComponent {

  constructor(private dsService: DataStorageService, private authService: AuthService) {}

  onSaveData() {
    this.dsService.storeRecipes().subscribe(
      (response: HttpEvent<Object>) => {
        console.log(response.type === HttpEventType.Sent);
      }
    );
  }

  onFetchData() {
    this.dsService.getRecipes();
  }

  onLogout() {
    this.authService.logout();
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
  }
}
