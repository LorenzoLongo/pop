# Bonus: Angular Universal

This folder contains all the information of section 25 of the Angular 7 Course.

## Logbook Section 25

    Lorenzo Longo - 22/04/2019
        > Create Section 25 Application
            -> Using Angular Universal
            -> Server-Side Build Workflow
            -> Add NodeJS Server
            -> Pre-Rendering the App on the Server
            -> Angular Universal Gotchas
        > Watch Course Videos 331-339
        > Writing Theory Section 25

## Links

> Angular Universal: https://angular.io/guide/universal