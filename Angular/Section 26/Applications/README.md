# Applications of Section 26

Here you can find all of the applications that are made inside section 26 of the Angular Course.

## Summary

    Animations Triggers and State
    Switching between States
    Transitions
    Advanced Transitions
    Tranisition Phases
    The "void" State
    Using Keyframes for Animations
    Grouing Transitions
    Using Animation Callbacks
    
#### Application Result
            
    Below you can find the result of the course project application
            
![Application result](./S26_Result_Course_Project.mp4)