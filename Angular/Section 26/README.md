# Angular Animations

This folder contains all the information of section 26 of the Angular 7 Course.

## Logbook Section 26

    Lorenzo Longo - 22/04/2019
        > Create Section 26 Application
            -> Animation Triggers & State
            -> Switching Between States
            -> Transitions
            -> Transition Phases
            -> void State
            -> Keyframes for Animations
            -> Grouping Transitions
            -> Animation Callbacks
        > Watch Course Videos 340-351
        > Writing Theory Section 26
        
    Lorenzo Longo - 23/04/2019
        > Update Section 26 Application

## Links

> Angular Animations: https://angular.io/guide/animations