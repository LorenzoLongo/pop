# Applications of Section 27

Here you can find all of the applications that are made inside section 27 of the Angular Course.

## Summary

    Adding a Service Worker
    Caching Assets for Offline Use
    Caching Dynamic Assets & URLs
    
#### Application Result
            
    Below you can find the result of the course project application
            
![Application result](./S27_Result_Course_Project.mp4)