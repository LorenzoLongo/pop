# Adding Offline Capabilities with Service Workers

This folder contains all the information of section 27 of the Angular 7 Course.

## Logbook Section 27

    Lorenzo Longo - 22/04/2019
        > Create Section 27 Application
            -> Adding Service Workers
            -> Caching Assets for Offline Use
            -> Caching Dynamic Assets & URLs
        > Watch Course Videos 352-356
        > Writing Theory Section 27

## Links

> Angular Service Workers: https://angular.io/guide/service-worker-intro

> Academic Resources: https://academind.com/learn/progressive-web-apps/