# Applications of Section 28

Here you can find all of the applications that are made inside section 28 of the Angular Course.

## Summary

    Analyzing the Testing Setup (created by the CLI)
    Running Tests (with the CLI)
    Adding Tests to for a certain Component
    Testing Dependencies: Components & Services
    Simulating Async Tasks
    Using "fakeAsync" and "tick"
    Isolated vs. Non-Isolated Tests
    