# A Basic Introduction to Unit Testing in Angular Apps

This folder contains all the information of section 28 of the Angular 7 Course.

## Logbook Section 28

    Lorenzo Longo - 23/04/2019
        > Create Section 28 Application
            -> Analyzing/Running CLI Test
            -> Testing Dependecies: Components & Services
            -> Simulating Async Tasks
            -> Using fakeAsync and tick
            -> Isolated vs. Non--Isolated Tests
        > Watch Course Videos 357-367
        > Writing Theory Section 28

## Links

> Angular Testing: https://angular.io/guide/testing

> Testing Components: https://semaphoreci.com/community/tutorials/testing-components-in-angular-2-with-jasmine

> Ng Test: https://github.com/angular/angular-cli/wiki/test

> e2e: https://github.com/angular/angular-cli/wiki/e2e