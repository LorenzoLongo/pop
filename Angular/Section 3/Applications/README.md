# Applications of Section 3

Here you can find all of the applications that are made inside section 3 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application

    Features the basics of Angular.
    
    
#### Application Structure

    Below you can find the structure of the application
    
![Application structure](./S3_Application_Structure.jpg)

#### Application Result

    Below you can find the result of the course project application
    
![Application result](./S3_Result_Course_Project.jpg)