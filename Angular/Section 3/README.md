# Course Project  - Angular Basics

This folder contains all the information of section 3 of the Angular 7 Course.

## Logbook Section 3

    Lorenzo Longo - 20/03/2019
        > Create Section 3 Application
            -> Installing Bootstrap
            -> Creating Components
            -> Adding a Navigation Bar
            -> Non-collapsable Navigation Bar
            -> Creating a Model
            -> ngFor (Output List of Data)
        > Watch Course Videos 42-58
        > Writing Theory Section 3
        
    Lorenzo Longo - 21/03/2019
        > Update Section 3 Application