# Applications of Section 31

Here you can find all of the applications that are made inside section 31 of the Angular Course.

## Summary

    Initializing the Project
    Setting up the Basic Project Files
    Installing the Core Dependencies
    Filling the Project files with some Life
    Index.html + Polyfills files
    Installing Development Dependencies
    Setting up a Development Workflow
    Update to Angular 6 + Webpack 4
    Finishing & Using the Development Workflow
    Setting up a Production Workflow
    Adding Types & Fixing Bugs
    Finishing Touches