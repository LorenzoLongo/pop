# Custom Project & Workflow Setup

This folder contains all the information of section 31 of the Angular 7 Course.


## Logbook Section 31

    Lorenzo Longo - 24/04/2019
        > Create Section 31 Application
            -> Initializing the Project
            -> Setup Basic Project Files
            -> Install Core Dependencies
            -> index.html + Polyfills
            -> Installing Development Dependencies
            -> Setup Development Workflow
            -> Update to Angular 6 - Webpack 4
            -> Using Development Workflow
            -> Setup Production Workflow
            -> Adding Types + Bugfixes
        > Watch Course Videos 372-384
        > Writing Theory Section 31

