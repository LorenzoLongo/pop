# Bonus: TypeScript Introduction (for Angular 2 Usage)

This folder contains all the information of section 32 of the Angular 7 Course.


## Logbook Section 32

    Lorenzo Longo - 26/04/2019
        > Create Section 32 Application
            -> Using Types
            -> Classes
            -> Interfaces
            -> Generics
            -> Modules
        > Watch Course Videos 385-391
        > Writing Theory Section 32
        
## Links

> TypeScript Handbook: http://www.typescriptlang.org/docs/home.html

> TypeScript Course: https://www.udemy.com/typescript/