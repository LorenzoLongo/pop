# Course Project  - Debugging in Angular

This folder contains all the information of section 4 of the Angular 7 Course.

## Theory

### Console Error Example

    1) File where you van find the error
    2) Lines where the error takes place (WARNING: after compiling these might not be right)
    3) Extra information about the error

![Console Error Example](./Debugging_Console_Error.jpg)

>TIP: Use Sourcemaps in the Chrome DevTools to debug (webpack folder)

### Augury 

>LINK: https://augury.rangle.io/

>Is an extension that can be installed inside Chrome and used inside the DevTools. This extension analyses your application. 

>It summarizes all of the components inside the Angular application and also reveals it's state with it's properties.

![Augury Components & State](./Augury_Components&State.jpg)

> It also reveals dependencies between components and services that are used inside these components.

![Injector Graph](./Injector_Graph.jpg)

> Inside the Router Tree Tab you can find all the routes inside of the application. 

![Router Tree](./Router_Tree.jpg)

> Also you can check all of the used ngModules inside of the application.

![NgModules](./NgModules.jpg)

## Logbook Section 4

    Lorenzo Longo - 22/03/2019
        > Write Section 4 Theory (GIT)
        > Watch Course Videos 59-61
        > Writing Theory Section 4