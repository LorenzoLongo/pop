# Applications of Section 5

Here you can find all of the applications that are made inside section 5 of the Angular Course.

## Summary

### Databinding

    Splitting an existing application into components
    Custom Property Binding
    Custom Event Binding
    Assigning an alias to a custom event
    Assigning an alias to a custom property
    View Encapsulation (console.log)
    Local references in templates
    @Viewchild access to DOM/template
    Ng-content projecting content inside components
    Component lifecycle hooks
    @ContentChild to access the ng-content content
    
    
    