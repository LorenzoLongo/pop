# Assignments of Section 5

Here you can find all of the assigments that are made inside section 5 of the Angular Course.

## Summary

### Assignment 1 - Components/Custom Event Binding/Custom Property Biding/Local References

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%205/Assignments/S5A1

    1) Create three new components: GameControl, Odd and Even
    2) The GameControl Component should have buttons to start and stop the game
    3) When starting the game, an event (holding a incrementing number) should get emitted each second (ref = setInterval())
    4) The event should be listenable from outside the component
    5) When stopping the game, no more events should get emitted (clearInterval(ref))
    6) A new Odd component should get created for every odd number emitted, the same should happen for the Even component (on even numbers)
    7) Simply output Odd - NUMBER or Even - NUMBER in the two components
    8) Style the element (e.g. paragraph) holding your output text differently in both components
    

![Assingment 1 result](./S5A1.mp4)