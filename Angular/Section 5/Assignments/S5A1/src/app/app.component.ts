import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  oddNumbers: number[] = [];
  evenNumbers: number[] = [];

  onIntervalExecuted(numberInterval: number) {
    if(numberInterval % 2 === 0) {
      this.evenNumbers.push(numberInterval);
    } else {
      this.oddNumbers.push(numberInterval);
    }
  }
}
