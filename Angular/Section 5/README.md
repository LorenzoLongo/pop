# Components & Deep Dive Databinding

This folder contains all the information of section 5 of the Angular 7 Course.

## Logbook Section 5

    Lorenzo Longo - 22/03/2019
        > Create Section 5 Application
            -> Binding Custom Properties
            -> Aliassing Custom Properties
            -> Binding Custom Events
            -> Aliassing Custom Events
            -> View Encapsulation
            -> @ViewChild() Usage
            -> Projecting Content with ngContent
            -> Component Lifecycle (ex. ngOninit)
            -> Lifecycle Hooks
            -> @ContentChild() Usage
        > Watch Course Videos 62-82
        > Writing Theory Section 5
        > Update Planning (GIT)
        > Update POP Main ReadMe (GIT)
        
    Lorenzo Longo - 23/03/2019
        > Create Assignment 1 of Section 5