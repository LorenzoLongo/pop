# Applications of Section 6

Here you can find all of the applications that are made inside section 6 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application

    Adding navigation with Event Binding and ngIf
    Passing recipe data with Property Binding
    Passing data with Event and Property Binding
    Add ingredients to the shopping list 
