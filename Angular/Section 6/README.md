# Course Project - Components & Deep Dive Databinding

This folder contains all the information of section 6 of the Angular 7 Course.

## Logbook Section 6

    Lorenzo Longo - 24/03/2019
        > Create Section 6 Application
            -> Navigation with Event Bidning & nfIf
            -> Passing Data with Property Binding
            -> Pass Data with Event & Property Binding
        > Watch Course Videos 83-88
        > Writing Theory Section 6
        
    Lorenzo Longo - 25/03/2019
        > Update Section 6 Application