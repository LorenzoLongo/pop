# Applications of Section 7

Here you can find all of the applications that are made inside section 7 of the Angular Course.

## Summary

### Deep Dive Directives

    Use ngFor and ngIf
    Use ngClass and ngStyle
    Create a basic attribute directive
    Using Renderer interface to build better attribute directives
    Using HostListener decorator (listens to host events)
    Using HostBinding decorator (bind to host properties)
    Directive Property Binding
    Building a structural directive
    Using ngSwitch
