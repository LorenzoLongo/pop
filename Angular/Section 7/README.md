# Deep Dive Directives

This folder contains all the information of section 7 of the Angular 7 Course.

## Logbook Section 7

    Lorenzo Longo - 26/03/2019
        > Create Section 7 Application
            -> ngFor & ngIf
            -> ngStyle & ngClass
            -> Create an Attribute Directive
            -> Renderer Usage
            -> HostListener (for Host Events)
            -> Hostbinding (to Host Properties)
            -> Binding Directive Properties
            -> Structural Directive
            -> ngSwitch
        > Watch Course Videos 89-100
        > Writing Theory Section 7        
        
## Links

> Renderer 2: https://angular.io/api/core/Renderer2