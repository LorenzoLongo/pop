# Applications of Section 8

Here you can find all of the applications that are made inside section 8 of the Angular Course.

## Summary

### Course Project - Recipe book and shopping list application

    Adding a structural directive (dropdown menu)
    
#### Application Result

    Below you can find the result of the course project application

![Application result](./S8_Result_Course_Project.jpg)