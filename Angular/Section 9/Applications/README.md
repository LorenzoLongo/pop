# Applications of Section 9

Here you can find all of the applications that are made inside section 9 of the Angular Course.

## Summary

### Services and Dependency Injection

    Creating a Logging Service
    Injecting the Logging Service into Components
    Creating a Data Service
    Injecting Services into a Services
    Using Services for Cross-Component Communication

#### Application Result

![Application result](./S9_Result_Course_Project.mp4)