# Assignments of Section 9

Here you can find all of the assigments that are made inside section 9 of the Angular Course.

## Summary

### Assignment 1 - 

>Link: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%209/Assignments/S9A1

    1) Add a Users Service
    2) Manage users globally
    3) Add a Counters Service
    
![Assignment 1 result](./S9A1.mp4)

