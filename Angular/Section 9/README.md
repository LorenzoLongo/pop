# Services & Dependency Injection

This folder contains all the information of section 9 of the Angular 7 Course.

## Logbook Section 9

    Lorenzo Longo - 29/03/2019
        > Create Section 9 Application
        > Create Assignment 1 of Section 9
            -> Creating a Service (Logging & Data Service)
            -> Injecting the Service into a Component
            -> Injecting Services into a Service
            -> Service Cross-Component Communication
        > Watch Course Videos 103-113
        > Writing Theory Section 9
        > Tussenrapportering
        
    Lorenzo Longo - 01/04/2019
        > Update Section 9 Application 

## Links

> Angular Services: https://angular.io/tutorial/toh-pt4