# Angular Bootstrap Example Application

This folder contains all the files concerning the project which I created for demonstrating what I've learned throughout the two courses. 

## Practical

This project uses a Firebase Database and Authentication. The project needs to be started with ng serve inside "kaagent" folder. 

## Project Result

### Part 1

![Project Result Part 1](Project_Part1.mp4)

### Part 2

![Project Result Part 1](Project_Part2.mp4)