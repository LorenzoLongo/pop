import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Player} from "../../team/player.model";
import {DataStorageService} from "../../shared/data-storage.service";
import {PlayerService} from "../../team/player.service";
import {Response} from '@angular/http';

@Component({
  selector: 'app-admin-add-player',
  templateUrl: './admin-add-player.component.html',
  styleUrls: ['./admin-add-player.component.css']
})
export class AdminAddPlayerComponent implements OnInit {
  positions: any[] = [
    {id: 0, name: 'Doelman'},
    {id: 1, name: 'Verdediger'},
    {id: 2, name: 'Middenvelder'},
    {id: 3, name: 'Aanvaller'}
  ];

  selected: number = 0;
  cleanSheets: string;
  player: Player[] = [];

  constructor(private dsService: DataStorageService, private playerService: PlayerService) {
    this.checkId();
  }

  ngOnInit() {
  }

  selectOption(id: number) {
    this.selected = id;
    this.checkId();
  }

  checkId() {
    if (this.selected == 0) return true;
    else return false;
  }

  onSubmit(form: NgForm) {
    const positionTable = this.getPositionTable(form.value.position);
    this.player['position'] = this.getPosition(form.value.position);
    this.player['name'] = form.value.fullName.toString();
    this.player['nationality'] = form.value.nationality.toString();
    this.player['dateOfBirth'] = form.value.dateOfBirth.toString();
    this.player['placeOfBirth'] = form.value.placeOfBirth.toString();
    this.player['contract'] = form.value.contract.toString();
    this.player['shirtNr'] = form.value.shirtNr.toString();
    this.player['games'] = form.value.games.toString();
    this.player['playtime'] = form.value.playtime.toString();

    if (form.value.position == 0) {
      this.player['cleanSheets'] = form.value.cleanSheets.toString();
    } else {
      this.player['cleanSheets'] = '0';
    }

    this.player['goals'] = form.value.goals.toString();
    this.player['assists'] = form.value.assists.toString();
    this.player['yellowCards'] = form.value.yellowCards.toString();
    this.player['redCards'] = form.value.redCards.toString();
    this.player['image'] = form.value.imageLink.toString();

    this.playerService.setPlayer(this.player);
    this.dsService.storePlayer(positionTable).subscribe(
      (response: Response) => {
        console.log(response)
      }
    );
  }

  getPositionTable(selectOption: number) {
    let positionString = this.getPosition(selectOption);

    if (positionString == 'Doelman') return 'keepers';
    else if (positionString == 'Verdediger') return 'defenders';
    else if (positionString == 'Middenvelder') return 'midfielders';
    else if (positionString == 'Aanvaller') return 'attackers';
  }

  getPosition(selectOption: number) {
    let positionString;
    for (let position of this.positions) {
      if (position.id == selectOption) positionString = position.name;
    }
    return positionString.toString();
  }
}
