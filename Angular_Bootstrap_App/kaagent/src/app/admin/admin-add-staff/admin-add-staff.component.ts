import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-admin-add-staff',
  templateUrl: './admin-add-staff.component.html',
  styleUrls: ['./admin-add-staff.component.css']
})
export class AdminAddStaffComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log('Add Staff member:\n\tName: ' + form.value.fullName + '\n\tFunction: ' + form.value.func + '\n\tLink to image: ' + form.value.imageLink);
  }
}
