import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AdminComponent} from "./admin.component";
import {AdminAddPlayerComponent} from "./admin-add-player/admin-add-player.component";
import {AdminAddStaffComponent} from "./admin-add-staff/admin-add-staff.component";
import {AuthGuardService} from "../auth/auth-guard.service";


const adminRoutes: Routes = [
  {
    path: 'admin', component: AdminComponent, canActivate: [AuthGuardService], children: [
      {path: 'add_player', component: AdminAddPlayerComponent,canActivate: [AuthGuardService]},
      {path: 'staff', component: AdminAddStaffComponent, canActivate: [AuthGuardService]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
