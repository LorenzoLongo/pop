import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  private currentRoute: string;
  private route: Router;

  constructor(private router: Router) {
    this.route = router;
    this.currentRoute = router.url.toString();
  }

  ngOnInit() {

  }

  checkCurrentRoute() {
    this.currentRoute = this.route.url.toString();
    return this.currentRoute;
  }
}
