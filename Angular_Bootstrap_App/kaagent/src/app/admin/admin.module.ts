import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {AppRoutingModule} from "../app-routing.module";
import {DataStorageService} from "../shared/data-storage.service";
import {AdminComponent} from "./admin.component";
import {AdminAddPlayerComponent} from "./admin-add-player/admin-add-player.component";
import {AdminAddStaffComponent} from "./admin-add-staff/admin-add-staff.component";
import {FormsModule} from "@angular/forms";
import {AdminRoutingModule} from "./admin-routing.module";
import {AuthGuardService} from "../auth/auth-guard.service";

@NgModule({
  declarations: [
    AdminComponent,
    AdminAddPlayerComponent,
    AdminAddStaffComponent
  ],
  imports: [
    SharedModule,
    FormsModule,
    AdminRoutingModule
  ],
  providers: [
    DataStorageService,
    AuthGuardService
  ]
})
export class AdminModule {
}
