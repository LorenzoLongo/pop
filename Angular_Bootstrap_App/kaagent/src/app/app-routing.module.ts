import {NgModule} from "@angular/core";
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./core/home/home.component";
import {CompetitionComponent} from "./competition/competition.component";
import {TeamComponent} from "./team/team.component";
import {ContactComponent} from "./contact/contact.component";
import {NewsArticleDetailedComponent} from "./news-article-detailed/news-article-detailed.component";

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'news/:id', component: NewsArticleDetailedComponent},
  {path: 'team', component: TeamComponent},
  {path: 'competition', component: CompetitionComponent},
  {path: 'contact', component: ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
