import {Component} from '@angular/core';
import {Title} from "@angular/platform-browser";
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'KAA Gent';

  public constructor(private titleService: Title) {
    this.setTitle();

    firebase.initializeApp({
      apiKey: "AIzaSyBnfImePDoJFdvtWlrIKGpZ0pqHonDWeN0",
      authDomain: "kaa-gent-7e816.firebaseapp.com",
      databaseURL: "https://kaa-gent-7e816.firebaseio.com/",
      projectId: "kaa-gent-7e816",
      storageBucket: "kaa-gent-7e816.appspot.com",
      messagingSenderId: "226430312561"
    });
  }

  public setTitle() {
    this.titleService.setTitle(this.title);
  }
}
