import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {CoreModule} from "./core/core.module";
import {SharedModule} from "./shared/shared.module";
import {HttpModule} from "@angular/http";
import {ContactModule} from "./contact/contact.module";
import {SponsorsModule} from "./sponsors/sponsors.module";
import {TeamModule} from "./team/team.module";
import {AuthModule} from "./auth/auth.module";
import {FormsModule} from "@angular/forms";
import {AdminModule} from "./admin/admin.module";
import {CompetitionModule} from "./competition/competition.module";
import { NewsArticleDetailedComponent } from './news-article-detailed/news-article-detailed.component';
import { SeasonTicketBannerComponent } from './news-article-detailed/season-ticket-banner/season-ticket-banner.component';


@NgModule({
  declarations: [
    AppComponent,
    NewsArticleDetailedComponent,
    SeasonTicketBannerComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    SharedModule,
    AppRoutingModule,
    SponsorsModule,
    CoreModule,
    TeamModule,
    CompetitionModule,
    ContactModule,
    AdminModule,
    AuthModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule {
}
