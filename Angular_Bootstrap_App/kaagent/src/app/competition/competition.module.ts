import {NgModule} from "@angular/core";
import {SharedModule} from "../shared/shared.module";
import {AppRoutingModule} from "../app-routing.module";
import {CompetitionComponent} from "./competition.component";
import {CompetitionRegularComponent} from "./competition-regular/competition-regular.component";
import {CompetitionPlayoffComponent} from "./competition-playoff/competition-playoff.component";


@NgModule({
  declarations: [
    CompetitionComponent,
    CompetitionRegularComponent,
    CompetitionPlayoffComponent
  ],
  imports: [
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    AppRoutingModule
  ],
  providers: [
  ]
})
export class CompetitionModule {
}
