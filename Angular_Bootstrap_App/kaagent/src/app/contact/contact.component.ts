import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private title: Title) {
    this.title.setTitle("KAA Gent | Contact");
  }

  ngOnInit() {

  }

  onSubmit(form: NgForm) {
    const name = form.value.name;
    const email = form.value.email;
    const message = form.value.message;

    console.log('Your name is: ' + name + '\n Your mail address is: ' + email + '\n Your message is: ' + message);
  }
}
