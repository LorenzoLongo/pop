import {NgModule} from "@angular/core";
import {ContactComponent} from "./contact.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {ContactLocationComponent} from './contact-location/contact-location.component';
import {ContactAdressesComponent} from './contact-adresses/contact-adresses.component';

@NgModule({
  declarations: [
    ContactComponent,
    ContactLocationComponent,
    ContactAdressesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
})
export class ContactModule {

}
