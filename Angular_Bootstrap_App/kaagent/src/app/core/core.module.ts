import {HomeComponent} from "./home/home.component";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "../app-routing.module";
import {HeaderComponent} from './header/header.component';
import {SharedModule} from "../shared/shared.module";
import {ArticleService} from "../shared/articles.service";
import {DataStorageService} from "../shared/data-storage.service";
import {FooterComponent} from './footer/footer.component';
import {NewsItemsComponent} from './home/news-items/news-items.component';
import {NewsItemsAsideComponent} from './home/news-items/news-items-aside/news-items-aside.component';
import {NewsItemsCardsComponent} from './home/news-items/news-items-cards/news-items-cards.component';
import {TwitterReactionsComponent} from './home/twitter-reactions/twitter-reactions.component';
import {DottingPipe} from "./home/news-items/news-items-cards/dotting.pipe";


@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    NewsItemsComponent,
    NewsItemsAsideComponent,
    NewsItemsCardsComponent,
    TwitterReactionsComponent,
    DottingPipe
  ],
  imports: [
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    AppRoutingModule,
    HeaderComponent,
    FooterComponent,
    DottingPipe
  ],
  providers: [
    ArticleService,
    DataStorageService,
  ]
})
export class CoreModule {
}
