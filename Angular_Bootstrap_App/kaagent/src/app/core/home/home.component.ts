import {Component, OnDestroy, OnInit} from '@angular/core';
import {Article} from "../../shared/article.model";
import {ArticleService} from "../../shared/articles.service";
import {Subscription} from "rxjs";
import {DataStorageService} from "../../shared/data-storage.service";
import {DomSanitizer, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  articles: Article[];
  subscription: Subscription;

  constructor(private articleService: ArticleService, private dsService: DataStorageService, public sanitizer: DomSanitizer, private title: Title) {
    this.title.setTitle("KAA Gent | Home");
  }

  ngOnInit() {
    this.dsService.getArticles();
    this.subscription = this.articleService.articlesChanged.subscribe(
      (recipes: Article[]) => {
        this.articles = recipes;
      }
    );
    this.articles = this.articleService.getArticle();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
