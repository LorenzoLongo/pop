import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dotting'
})
export class DottingPipe implements PipeTransform {

  transform(value: any) {
    return value + '...';
  }

}
