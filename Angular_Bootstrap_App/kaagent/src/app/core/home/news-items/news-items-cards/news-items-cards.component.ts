import {Component, OnDestroy, OnInit} from '@angular/core';
import {Article} from "../../../../shared/article.model";
import {Subscription} from "rxjs";
import {ArticleService} from "../../../../shared/articles.service";
import {DataStorageService} from "../../../../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Router} from "@angular/router";

@Component({
  selector: 'app-news-items-cards',
  templateUrl: './news-items-cards.component.html',
  styleUrls: ['./news-items-cards.component.css']
})
export class NewsItemsCardsComponent implements OnInit, OnDestroy {
  articles: Article[];
  subscription: Subscription;

  constructor(private articleService: ArticleService, private dsService: DataStorageService, public sanitizer: DomSanitizer, private router: Router) {
  }

  ngOnInit() {
    this.dsService.getArticles();
    this.subscription = this.articleService.articlesChanged.subscribe(
      (recipes: Article[]) => {
        this.articles = recipes;
      }
    );
    this.articles = this.articleService.getArticle();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  navigateToArticle(index: number) {
    this.router.navigate(['/news/' + index]);
  }

}
