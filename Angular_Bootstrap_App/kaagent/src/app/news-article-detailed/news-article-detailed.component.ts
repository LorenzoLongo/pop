import {Component, OnDestroy, OnInit} from '@angular/core';
import {Article} from "../shared/article.model";
import {Subscription} from "rxjs";
import {ArticleService} from "../shared/articles.service";
import {DataStorageService} from "../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Router} from "@angular/router";

@Component({
  selector: 'app-news-article-detailed',
  templateUrl: './news-article-detailed.component.html',
  styleUrls: ['./news-article-detailed.component.css']
})
export class NewsArticleDetailedComponent implements OnInit, OnDestroy {
  articles: Article[];
  subscription: Subscription;
  private routeIndex: number;

  constructor(private articleService: ArticleService, private dsService: DataStorageService, public sanitizer: DomSanitizer, private router: Router) {
  }

  ngOnInit() {
    this.getRouteIndex();
    this.dsService.getArticles();
    this.subscription = this.articleService.articlesChanged.subscribe(
      (recipes: Article[]) => {
        this.articles = recipes;
      }
    );
    this.articles = this.articleService.getArticle();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private getRouteIndex() {
    this.routeIndex = parseInt(this.router.url.substr(6, this.router.url.length));
  }
}
