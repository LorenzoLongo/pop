export class Article {
  public title: string;
  public headerImage: string;
  public contentTitle: string;
  public content: string;
  public imagePaths: string[];
  public video: string;

  constructor(title: string, headerImage: string, contentTitle: string, content: string, video: string, imagePaths: string[]) {
    this.title = title;
    this.headerImage = headerImage;
    this.contentTitle = contentTitle;
    this.content = content;
    this.video = video;
    this.imagePaths = imagePaths;
  }
}
