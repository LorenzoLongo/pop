import {Injectable} from "@angular/core";
import {Article} from "./article.model";
import {Subject} from "rxjs";

@Injectable()
export class ArticleService {
  articlesChanged = new Subject<Article[]>();

  private articles: Article[];

  setArticles(articles: Article[]) {
    this.articles = articles;
    this.articlesChanged.next(this.articles.slice());
  }

  getArticle() {
    return this.articles;
  }
}
