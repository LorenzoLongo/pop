import {Injectable} from "@angular/core";
import {Article} from "./article.model";
import {Http, Response} from "@angular/http";
import 'rxjs/Rx';
import {ArticleService} from "./articles.service";
import {SponsorsService} from "../sponsors/sponsors.service";
import {Sponsors} from "../sponsors/sponsors.model";
import {Player} from "../team/player.model";
import {PlayerService} from "../team/player.service";
import {Staff} from "../team/staff.model";
import {StaffService} from "../team/staff.service";
import {AuthService} from "../auth/auth.service";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class DataStorageService {
  constructor(private http: Http, private articleService: ArticleService, private sponsorsService: SponsorsService, private playerService: PlayerService, private  staffService: StaffService, private authService: AuthService) {
  }

  storePlayer(position: string) {
    const token = this.authService.getToken();

    return this.http.patch('https://kaa-gent-7e816.firebaseio.com/' + position + '.json',
      {
        '4':
          {
            'name': this.playerService.getPlayer()['name'],
            'image': this.playerService.getPlayer()['image'],
            'position': this.playerService.getPlayer()['position'],
            'shirtNr': this.playerService.getPlayer()['shirtNr'],
            'nationality': this.playerService.getPlayer()['nationality'],
            'dateOfBirth': this.playerService.getPlayer()['dateOfBirth'],
            'placeOfBirth': this.playerService.getPlayer()['placeOfBirth'],
            'contract': this.playerService.getPlayer()['contract'],
            'games': this.playerService.getPlayer()['games'],
            'playtime': this.playerService.getPlayer()['playtime'],
            'goals': this.playerService.getPlayer()['goals'],
            'yellowCards': this.playerService.getPlayer()['yellowCards'],
            'redCards': this.playerService.getPlayer()['redCards'],
            'cleanSheets': this.playerService.getPlayer()['cleanSheets'],
            'assists': this.playerService.getPlayer()['assists'],
          }
      });
  }


  getArticles() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/articles.json').map(
      (response: Response) => {
        const articles: Article[] = response.json();
        for (let article of articles) {
          if (!article['imagePaths']) {
            article['imagePaths'] = [];
          }
          if (!article['video']) {
            article['video'] = '';
          }
          if (!article['content']) {
            article['content'] = '';
          }
        }
        return articles;
      }
    ).subscribe(
      (articles: Article[]) => {
        this.articleService.setArticles(articles);
      }
    );
  }

  getSponsors() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/sponsors.json').map(
      (response: Response) => {
        const sponsors: Sponsors[] = response.json();

        return sponsors;
      }
    ).subscribe(
      (sponsors: Sponsors[]) => {
        this.sponsorsService.setSponsors(sponsors);
      }
    );
  }

  getKeepers() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/keepers.json').map(
      (response: Response) => {
        const keepers: Player[] = response.json();

        return keepers;
      }
    ).subscribe(
      (keepers: Player[]) => {
        this.playerService.setKeepers(keepers);
      }
    );
  }

  getDefenders() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/defenders.json').map(
      (response: Response) => {
        const defenders: Player[] = response.json();

        return defenders;
      }
    ).subscribe(
      (defenders: Player[]) => {
        this.playerService.setDefenders(defenders);
      }
    );
  }

  getMidfielders() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/midfielders.json').map(
      (response: Response) => {
        const midfielders: Player[] = response.json();

        return midfielders;
      }
    ).subscribe(
      (midfielders: Player[]) => {
        this.playerService.setMidfielders(midfielders);
      }
    );
  }

  getAttackers() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/attackers.json').map(
      (response: Response) => {
        const attackers: Player[] = response.json();

        return attackers;
      }
    ).subscribe(
      (attackers: Player[]) => {
        this.playerService.setAttackers(attackers);
      }
    );
  }

  getStaffMembers() {
    return this.http.get('https://kaa-gent-7e816.firebaseio.com/staff.json').map(
      (response: Response) => {
        const staff: Staff[] = response.json();

        return staff;
      }
    ).subscribe(
      (staff: Staff[]) => {
        this.staffService.setStaffMembers(staff);
      }
    );
  }
}

