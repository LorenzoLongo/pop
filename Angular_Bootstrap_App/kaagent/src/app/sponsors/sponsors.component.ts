import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {DataStorageService} from "../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Sponsors} from "./sponsors.model";
import {SponsorsService} from "./sponsors.service";

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.css']
})
export class SponsorsComponent implements OnInit {
  sponsors: Sponsors[];
  subscription: Subscription;

  constructor(private sponsorsService: SponsorsService, private dsService: DataStorageService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dsService.getSponsors();
    this.subscription = this.sponsorsService.sponsorsChanged.subscribe(
      (sponsors: Sponsors[]) => {
        this.sponsors = sponsors;
      }
    );
    this.sponsors = this.sponsorsService.getSponsor();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
