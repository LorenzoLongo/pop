export class Sponsors {
  public imageLink: string;
  public webLink: string;

  constructor(imageLink: string, webLink: string) {
    this.imageLink = imageLink;
    this.webLink = webLink;
  }
}
