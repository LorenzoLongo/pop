import {NgModule} from "@angular/core";
import {AppRoutingModule} from "../app-routing.module";
import {SharedModule} from "../shared/shared.module";
import {DataStorageService} from "../shared/data-storage.service";
import {SponsorsComponent} from "./sponsors.component";
import {SponsorsService} from "./sponsors.service";

;


@NgModule({
  declarations: [
    SponsorsComponent
  ],
  imports: [
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    AppRoutingModule,
    SponsorsComponent
  ],
  providers: [
    SponsorsService,
    DataStorageService,
  ]
})
export class SponsorsModule {
}
