import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Sponsors} from "./sponsors.model";

@Injectable()
export class SponsorsService {
  sponsorsChanged = new Subject<Sponsors[]>();

  private sponsors: Sponsors[];

  setSponsors(sponsors: Sponsors[]) {
    this.sponsors = sponsors;
    this.sponsorsChanged.next(this.sponsors.slice());
  }

  getSponsor() {
    return this.sponsors;
  }
}
