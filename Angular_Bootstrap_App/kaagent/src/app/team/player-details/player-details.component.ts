import {Component, Input, OnInit} from '@angular/core';
import {Player} from "../player.model";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.css']
})
export class PlayerDetailsComponent implements OnInit {
  @Input() keepers: Player;
  @Input() defenders: Player;
  @Input() midfielders: Player;
  @Input() attackers: Player;
  @Input() index: number;
  @Input() typeOfPlayer: number;

  constructor(public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

}
