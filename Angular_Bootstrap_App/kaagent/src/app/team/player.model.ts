export class Player {
  public name: string;
  public image: string;
  public position: string;
  public shirtNr: string;
  public nationality: string;
  public dateOfBirth: string;
  public placeOfBirth: string;
  public contract: string;

  public games: string;
  public playtime: string;
  public goals: string;
  public yellowCards: string;
  public redCards: string;
  public cleanSheets: string;
  public assists: string;


  constructor(name: string, image: string, position: string, shirtNr: string, nationality: string, dateOfBirth: string, placeOfBirth: string, contract: string, games: string, playtime: string, goals: string, yellowCards: string, redCards: string, cleanSheets: string, assists: string) {
    this.name = name;
    this.image = image;
    this.position = position;
    this.shirtNr = shirtNr;
    this.nationality = nationality;
    this.dateOfBirth = dateOfBirth;
    this.placeOfBirth = placeOfBirth;
    this.contract = contract;
    this.games = games;
    this.playtime = playtime;
    this.goals = goals;
    this.yellowCards = yellowCards;
    this.redCards = redCards;
    this.cleanSheets = cleanSheets;
    this.assists = assists;
  }
}
