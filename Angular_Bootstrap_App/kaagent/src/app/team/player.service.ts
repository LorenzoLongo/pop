import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Player} from "./player.model";

@Injectable()
export class PlayerService {
  keepersChanged = new Subject<Player[]>();
  defendersChanged = new Subject<Player[]>();
  attackersChanged = new Subject<Player[]>();
  midfieldersChanged = new Subject<Player[]>();
  playersChanged = new Subject<Player[]>();

  private defenders: Player[];
  private keepers: Player[];
  private midfielders: Player[];
  private attackers: Player[];
  private player: Player[];

  setDefenders(defenders: Player[]) {
    this.defenders = defenders;
    this.defendersChanged.next(this.defenders.slice());
  }

  getDefenders() {
    return this.defenders;
  }

  setKeepers(keepers: Player[]) {
    this.keepers = keepers;
    this.keepersChanged.next(this.keepers.slice());
  }

  getKeepers() {
    return this.keepers;
  }

  setAttackers(attackers: Player[]) {
    this.attackers = attackers;
    this.attackersChanged.next(this.attackers.slice());
  }

  getAttackers() {
    return this.attackers;
  }

  setMidfielders(midfielders: Player[]) {
    this.midfielders = midfielders;
    this.midfieldersChanged.next(this.midfielders.slice());
  }

  getMidfielders() {
    return this.midfielders;
  }

  setPlayer(player: Player[]) {
    this.player = player;
    this.playersChanged.next(this.player.slice());
  }

  getPlayer() {
    return this.player;
  }
}
