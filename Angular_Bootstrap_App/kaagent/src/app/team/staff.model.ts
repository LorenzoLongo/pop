export class Staff {
  public name: string;
  public image: string;
  public func: string;

  constructor(name: string, image: string, func: string) {
    this.name = name;
    this.image = image;
    this.func = func;
  }
}
