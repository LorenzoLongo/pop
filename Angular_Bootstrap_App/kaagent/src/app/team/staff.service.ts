import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Staff} from "./staff.model";

@Injectable()
export class StaffService {
  staffChanged = new Subject<Staff[]>();

  private staff: Staff[];

  setStaffMembers(staff: Staff[]) {
    this.staff = staff;
    this.staffChanged.next(this.staff.slice());
  }

  getStaffMembers() {
    return this.staff;
  }
}
