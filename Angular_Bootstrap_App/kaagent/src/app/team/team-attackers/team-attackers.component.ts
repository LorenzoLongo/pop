import {Component, OnDestroy, OnInit} from '@angular/core';
import {Player} from "../player.model";
import {Subscription} from "rxjs";
import {PlayerService} from "../player.service";
import {DataStorageService} from "../../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-team-attackers',
  templateUrl: './team-attackers.component.html',
  styleUrls: ['./team-attackers.component.css']
})
export class TeamAttackersComponent implements OnInit, OnDestroy {
  attackers: Player[];
  subscription: Subscription;

  constructor(private playerService: PlayerService, private dsService: DataStorageService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dsService.getAttackers();
    this.subscription = this.playerService.attackersChanged.subscribe(
      (attackers: Player[]) => {
        this.attackers = attackers;
      }
    );
    this.attackers = this.playerService.getAttackers();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
