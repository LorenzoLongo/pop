import {Component, OnDestroy, OnInit} from '@angular/core';
import {Player} from "../player.model";
import {Subscription} from "rxjs";
import {PlayerService} from "../player.service";
import {DataStorageService} from "../../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-team-defenders',
  templateUrl: './team-defenders.component.html',
  styleUrls: ['./team-defenders.component.css']
})
export class TeamDefendersComponent implements OnInit, OnDestroy {
  defenders: Player[];
  subscription: Subscription;

  constructor(private playerService: PlayerService, private dsService: DataStorageService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dsService.getDefenders();
    this.subscription = this.playerService.defendersChanged.subscribe(
      (defenders: Player[]) => {
        this.defenders = defenders;
      }
    );
    this.defenders = this.playerService.getDefenders();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
