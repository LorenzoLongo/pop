import {Component, OnDestroy, OnInit} from '@angular/core';
import {Player} from "../player.model";
import {Subscription} from "rxjs";
import {PlayerService} from "../player.service";
import {DataStorageService} from "../../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-team-keepers',
  templateUrl: './team-keepers.component.html',
  styleUrls: ['./team-keepers.component.css']
})
export class TeamKeepersComponent implements OnInit, OnDestroy {
  keepers: Player[];
  subscription: Subscription;

  constructor(private playerService: PlayerService, private dsService: DataStorageService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dsService.getKeepers();
    this.subscription = this.playerService.keepersChanged.subscribe(
      (keepers: Player[]) => {
        this.keepers = keepers;
      }
    );
    this.keepers = this.playerService.getKeepers();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
