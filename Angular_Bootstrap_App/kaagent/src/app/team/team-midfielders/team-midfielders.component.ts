import {Component, OnDestroy, OnInit} from '@angular/core';
import {Player} from "../player.model";
import {Subscription} from "rxjs";
import {PlayerService} from "../player.service";
import {DataStorageService} from "../../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-team-midfielders',
  templateUrl: './team-midfielders.component.html',
  styleUrls: ['./team-midfielders.component.css']
})
export class TeamMidfieldersComponent implements OnInit, OnDestroy {
  midfielders: Player[];
  subscription: Subscription;

  constructor(private playerService: PlayerService, private dsService: DataStorageService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dsService.getMidfielders();
    this.subscription = this.playerService.midfieldersChanged.subscribe(
      (midfielders: Player[]) => {
        this.midfielders = midfielders;
      }
    );
    this.midfielders = this.playerService.getMidfielders();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
