import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {DataStorageService} from "../../shared/data-storage.service";
import {DomSanitizer} from "@angular/platform-browser";
import {StaffService} from "../staff.service";
import {Staff} from "../staff.model";

@Component({
  selector: 'app-team-staff',
  templateUrl: './team-staff.component.html',
  styleUrls: ['./team-staff.component.css']
})
export class TeamStaffComponent implements OnInit, OnDestroy {
  staff: Staff[];
  subscription: Subscription;

  constructor(private staffService: StaffService, private dsService: DataStorageService, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dsService.getStaffMembers();
    this.subscription = this.staffService.staffChanged.subscribe(
      (staff: Staff[]) => {
        this.staff = staff;
      }
    );
    this.staff = this.staffService.getStaffMembers();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
