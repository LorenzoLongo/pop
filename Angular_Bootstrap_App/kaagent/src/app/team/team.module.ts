import {NgModule} from "@angular/core";
import {TeamComponent} from "./team.component";
import {TeamKeepersComponent} from "./team-keepers/team-keepers.component";
import {TeamDefendersComponent} from "./team-defenders/team-defenders.component";
import {TeamMidfieldersComponent} from "./team-midfielders/team-midfielders.component";
import {TeamAttackersComponent} from "./team-attackers/team-attackers.component";
import {TeamStaffComponent} from "./team-staff/team-staff.component";
import {SharedModule} from "../shared/shared.module";
import {AppRoutingModule} from "../app-routing.module";
import {PlayerService} from "./player.service";
import {DataStorageService} from "../shared/data-storage.service";
import {StaffService} from "./staff.service";
import {PlayerDetailsComponent} from './player-details/player-details.component';

@NgModule({
  declarations: [
    TeamComponent,
    TeamKeepersComponent,
    TeamDefendersComponent,
    TeamMidfieldersComponent,
    TeamAttackersComponent,
    TeamStaffComponent,
    PlayerDetailsComponent
  ],
  imports: [
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    AppRoutingModule,
    TeamComponent
  ],
  providers: [
    PlayerService,
    StaffService,
    DataStorageService,
  ]
})
export class TeamModule {
}
