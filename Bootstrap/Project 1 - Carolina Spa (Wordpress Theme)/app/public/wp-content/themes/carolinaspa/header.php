<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link stylesheet="style.css">
    <title><?php wp_title(''); if(wp_title('', false)) { echo ' : '; } bloginfo('name') . " : " . bloginfo('description'); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Add Your HTML here -->
<header class="site-header container">
    <div class="row justify-content-center justify-content-lg-between">
        <div class="col-8 col-lg-4">
            <a href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" class="img-fluid d-block mx-auto">
        </div>
        <div class="col-12 col-lg-4">
            <?php
            $args = array(
                'container' => 'nav',
                'container_class' => 'socials text-center text-md-right pt-3',
                'link-before' => '<span class="sr-only">',
                'link-after' => '</span>',
                'theme_location' => 'social_menu'
            );

            wp_nav_menu($args);
            ?>
        </div>
    </div> <!-- Justify-Content-Between -->
</header>

<div class="navigation mt-4 py-1">
    <?php
    $args = array(
        'menu_class' => 'nav nav-justified flex-column flex-sm-row',
        'container' => 'collapse navbar-collapse',
        'container_id' => 'main-navigation',
        'theme_location' => 'main_menu'
    );

    wp_nav_menu($args);
    ?>
</div>