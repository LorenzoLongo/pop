// Add Your Scripts here
$ = jQuery.noConflict();

$(document).ready(function () {

    // Fixed menu on scroll

    var $mainNavHeight = $('.navigation').innerHeight();
    var $mainNavOffset = $('.navigation').offset();
    var $realDistance = $mainNavOffset.top = + $mainNavHeight;

    $(window).scroll(function () {
        var $scroll = $(window).scrollTop();

        if($scroll > $realDistance) {
            $('.navigation').addClass('fixed-top').removeClass('mt-4');
            $('body').css({'margin-top' : $mainNavHeight + 'px'});
        } else {
            $('.navigation').addClass('mt-4').removeClass('fixed-top');
            $('body').css({'margin-top' : '0px'});
        }
    });

    $('#contact_form').on('submit', function (event) {
        event.preventDefault();

        var $errors = [];

        var $name = $('#name').val();
        var $nameField = $('#name');
        var $nameDiv = $nameField.parent();
        var $validationName = $('#validationName');

        if($name.length < 4) {
            $nameField.addClass('is-invalid');
            $nameDiv.find('label').addClass('text-danger');
            $nameDiv.append('<small id="validationName" class="form-text text-muted">Not a valid name!</small>');
            $errors.push('1');
        } else {
            $nameField.addClass('is-valid').removeClass('is-invalid');
            $nameDiv.find('label').addClass('text-success').removeClass('text-danger');
            $validationName.remove();
        }


        var $email = $('#email').val();
        var $emailField = $('#email');
        var $emailDiv = $emailField.parent();
        var $validationEmail = $('#validationEmail');

        if($email.length < 5) {
            $emailField.addClass('is-invalid');
            $emailDiv.find('label').addClass('text-danger');
            $emailDiv.append('<small id="validationEmail" class="form-text text-muted">Not a valid email!</small>');
            $errors.push('2');
        } else {
            $emailField.addClass('is-valid').removeClass('is-invalid');
            $emailDiv.find('label').addClass('text-success').removeClass('text-danger');
            $validationEmail.remove();
        }


        var $message = $('#message').val();
        var $messageField = $('#message');
        var $messageDiv = $messageField.parent();
        var $validationMessage = $('#validationMessage');

        if($message.length < 10) {
            $messageField.addClass('is-invalid');
            $messageDiv.find('label').addClass('text-danger');
            $messageDiv.append('<small id="validationMessage" class="form-text text-muted">Not a valid message!</small>');
            $errors.push('3');
        } else {
            $messageField.addClass('is-valid').removeClass('is-invalid');
            $messageDiv.find('label').addClass('text-success').removeClass('text-danger');
            $validationMessage.remove();
        }


        var $contactForm = $('#contact_form');

        if(!$errors.length > 0) {
            $.ajax({
                type: $contactForm.attr('method'),
                url: $contactForm.attr('action'),
                data: $contactForm.serialize()
            }).done(function (data) {
                var $result = data;
                var $response = JSON.parse($result);
                $('#output').append($response.message).fadeIn().addClass('d-block');
            });
        }
    });
});