<?php get_header(); ?>

<?php
while(have_posts()): the_post();
    ?>

    <div class="container pt-4">
        <div class="row no-gutters">
            <div class="col-12 hero">
                <img src="img/about_us.jpg" class="img-fluid">
                <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                <h2 class="text-uppercase d-md-block d-none"><?php the_title() ?></h2>
            </div>
        </div>
    </div>

    <div class="container py-4">
        <div class="row">
            <main class="col-lg-8 main-content">
                <h2 class="d-block d-md-none text-center text-uppercase"><?php the_title() ?></h2>
                <div id="services" role="tablist" aria-multiselectable="true">
                    <div class="card">
                        <div class="card-header py-2" role="tab" id="service_1">
                            <h3 class="mb-0">
                                <a data-toggle="collapse" data-parent="#services" href="#service_1desc" aria-expanded="true" aria-controls="service_1desc"><?php the_field('service_1_title'); ?></a>
                            </h3>
                        </div>

                        <div id="service_1desc" class="collapse show" role="tabpanel" aria-labelledby="service_1">
                            <div class="card-block m-3">
                                <?php the_field('service_1_description'); ?>
                            </div><!-- card block-->
                        </div><!-- service_1desc -->
                    </div><!-- card -->


                    <div class="card">
                        <div class="card-header py-2" role="tab" id="service_2">
                            <h3 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#services" href="#service_2desc" aria-expanded="false" aria-controls="service_2desc"><?php the_field('service_2_title'); ?></a>
                            </h3>
                        </div>

                        <div id="service_2desc" class="collapse" role="tabpanel" aria-labelledby="service_2">
                            <div class="card-block m-3">
                                <?php the_field('service_2_description'); ?>
                            </div><!-- card block-->
                        </div><!-- service_1desc -->
                    </div><!-- card -->



                    <div class="card">
                        <div class="card-header py-2" role="tab" id="service_3">
                            <h3 class="mb-0">
                                <a class="collapsed" data-toggle="collapse" data-parent="#services" href="#service_3desc" aria-expanded="false" aria-controls="service_3desc"><?php the_field('service_3_title'); ?></a>
                            </h3>
                        </div>

                        <div id="service_3desc" class="collapse" role="tabpanel" aria-labelledby="service_3">
                            <div class="card-block m-3">
                                <?php the_field('service_3_description'); ?>
                            </div><!-- card block-->
                        </div><!-- service_1desc -->
                    </div><!-- card -->

                </div><!-- services -->
            </main>

            <?php get_sidebar(); ?>

        </div>
    </div>

<?php
endwhile;
?>

<?php get_template_part('templates/appointment'); ?>

<?php get_footer(); ?>
