# Bootstrap 4 - The Complete Guide By Building 8 Projects

This folder contains all the information of the Bootstrap initiation course.

## Project 1 - Carolina Spa

## Logbook 

    Lorenzo Longo - 04/03/2019
        > Watching course videos 1-6
        > Create first Angular application
        > Try to upload to GIT -> (couldn't push from Webstorm)
         
    Lorenzo Longo - 07/03/2019
        > Watching course 1-6 (second/third attempt)
        > Create first Angular application (second/third attempt)
        > Upload to GIT
        > Watching course videos 7-11
        
        
##LINKS

>Push to GIT: https://www.jetbrains.com/help/webstorm/commit-and-push-changes.html
 
>Angular File Structure: https://angular.io/guide/file-structure

>Angular Theory: https://angular.io/