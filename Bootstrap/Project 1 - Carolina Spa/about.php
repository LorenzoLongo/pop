<?php
    $title = 'About Us';
    include 'templates/header.php';
    include 'templates/navigation.php';
?>

<div class="container pt-4">
    <div class="row no-gutters">
        <div class="col-12 hero">
            <img src="img/about_us.jpg" class="img-fluid">
            <h2 class="text-uppercase d-md-block d-none">About Us</h2>
        </div>
    </div>
</div>

<div class="container py-4">
    <div class="row">
        <main class="col-lg-8 main-content">
            <h2 class="d-block d-md-none text-center text-uppercase">About Us</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium consectetur dolor, dolorem ea earum facilis id maiores molestias nam nesciunt perferendis placeat praesentium sequi sint sunt, ullam voluptates voluptatibus?Aliquid odio reprehenderit tempora velit. Accusantium amet consequatur dolores eaque, eius esse, fugit inventore laborum nemo officiis possimus provident quos reiciendis similique tempora tempore tenetur veritatis voluptas? Quas ratione, ullam.A adipisci aut commodi dolor dolores earum eligendi enim error esse exercitationem, expedita harum ipsum iusto minus modi molestias odio provident quaerat quas reiciendis repellat suscipit tempora veritatis vero voluptates.Ad asperiores beatae dolorem fugiat incidunt sunt voluptatibus. Architecto consequatur dignissimos dolor ea earum in laborum optio perspiciatis quo, recusandae sed tempore ullam. Consectetur dolor doloremque inventore nostrum perspiciatis quam.Aliquam at doloribus fugit perspiciatis possimus. Ab aliquam eaque eos, et hic illum inventore nisi officiis omnis quam quos reiciendis repudiandae similique soluta tempore, ullam unde ut vero vitae voluptate?</p>


            <div class="facilities-gallery">
                <h3 class="text-center text-uppercase p-4"><span>Check out our</span> facilities</h3>

                <a href="#" data-target="#image_1" data-toggle="modal">
                    <img src="img/gallery_thumb_01.jpg" class="rounded">
                </a>

                <a href="#" data-target="#image_2" data-toggle="modal">
                    <img src="img/gallery_thumb_02.jpg" class="rounded">
                </a>

                <a href="#" data-target="#image_3" data-toggle="modal">
                    <img src="img/gallery_thumb_03.jpg" class="rounded">
                </a>

                <div class="modal fade" id="image_1" tabindex="-1" role="dialog" aria-labelledby="image_1" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="img/gallery_large_01.jpg" class="img-fluid">
                            </div><!-- modal-body -->
                        </div><!-- modal-content -->
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

                <div class="modal fade" id="image_2" tabindex="-1" role="dialog" aria-labelledby="image_2" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="img/gallery_large_02.jpg" class="img-fluid">
                            </div><!-- modal-body -->
                        </div><!-- modal-content -->
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

                <div class="modal fade" id="image_3" tabindex="-1" role="dialog" aria-labelledby="image_3" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="img/gallery_large_03.jpg" class="img-fluid">
                            </div><!-- modal-body -->
                        </div><!-- modal-content -->
                    </div><!-- modal-dialog -->
                </div><!-- modal -->

            </div>
        </main>
        <aside class="col-lg-4 pt-4 pt-lg-0">
            <div class="sidebar hours p-3">
                <?php include 'templates/business-hours.php'; ?>
            </div>
        </aside>
    </div>
</div>

<?php

    include 'templates/footer.php';

?>
