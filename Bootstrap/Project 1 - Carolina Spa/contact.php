<?php
    $title = 'Contact Us';
    include 'templates/header.php';
    include 'templates/navigation.php';
?>

<div class="container pt-4">
    <div class="row no-gutters">
        <div class="col-12 hero">
            <img src="img/contact_us.jpg" class="img-fluid">
            <h2 class="text-uppercase d-md-block d-none">Contact Us</h2>
        </div>
    </div>
</div>

<div class="container py-4">
    <div class="row justify-content-center">
        <main class="col-md-10 main-content">
            <h2 class="d-block d-md-none text-center text-uppercase">Contact Us</h2>
            <form id="contact_form" action="send.php" method="post" class="p-5 contact-form mt-5">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Your Email">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea class="form-control" name="message" id="message" rows="6"></textarea>
                </div>
                <input type="submit" class="btn btn-primary text-uppercase" name="submit" value="Submit">
                <div id="output" class="alert alert-success text-center mt-3 d-none"></div>
            </form>
        </main>
    </div>
</div>

<?php

    include 'templates/footer.php';

?>
