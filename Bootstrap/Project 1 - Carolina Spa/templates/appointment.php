<div class="appointment container-fluid py-5">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-6 col-md-8 py-3 text-center">
            <h3 class="text-uppercase">Make an appointment</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab, architecto deleniti deserunt dolore ducimus excepturi expedita, facere ipsum nemo officiis placeat, quam quasi quos rerum sed sint soluta tempora!</p>
            <a href="#" class="btn btn-primary btn-lg mt-3 text-uppercase">read more</a>
        </div><!-- Col 12 -->
    </div><!-- .row -->
</div><!-- .container -->