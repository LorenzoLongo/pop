<footer class="site-footer pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3 class="text-uppercase text-center pb-4">About Us</h3>
                <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet assumenda cupiditate delectus deleniti dolore esse et excepturi fugit impedit iusto, laboriosam maiores possimus quas quisquam ratione recusandae sint totam voluptate!</p>
            </div>

            <div class="col-md-4 pb-4 pb-md-0">
                <h3 class="text-uppercase text-center pb-4">Open Hours</h3>
                <p class="text-center mb-0">Mon-Fri: 9 AM - 7 PM</p>
                <p class="text-center mb-0">Saturday: 10 AM - 2 PM</p>
                <p class="text-center mb-0">Sunday: Closed</p>
            </div>

            <div class="col-md-4">
                <h3 class="text-uppercase text-center pb-4">Contact</h3>
                <p class="mb-0 text-center">66 East Sunnyslope Avenue</p>
                <p class="text-center">Lansdowne, PA 19050</p>

                <div class="social-nav">
                    <nav class="socials text-center pt-3">
                        <ul class="p-0">
                            <li><a href="http://facebook.com"><span class="sr-only">Facebook</span></a></li>
                            <li><a href="http://twitter.com"><span class="sr-only">Twitter</span></a></li>
                            <li><a href="http://instagram.com"><span class="sr-only">Instagram</span></a></li>
                            <li><a href="http://pinterest.com"><span class="sr-only">Pinterest</span></a></li>
                            <li><a href="http://youtube.com"><span class="sr-only">YouTube</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>


            <div class="w-100">

            </div>

            <hr class="w-100">

            <p class="copyright text-center w-100">Carolina Spa & Salon 2019</p>

        </div><!-- .row -->
    </div><!-- .container -->
</footer>

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</body>
</html>