<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/fontawesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Italianno|Lato:400,700,900|Raleway:400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/styles.css">
    <title>Carolina Spa | <?php echo $title ?></title>
</head>
<body>
<!-- Add Your HTML here -->
<header class="site-header container">
    <div class="row justify-content-center justify-content-lg-between">
        <div class="col-8 col-lg-4">
            <a href="index.php">
            <img src="img/logo.svg" class="img-fluid d-block mx-auto">
        </div>
        <div class="col-12 col-lg-4">
            <nav class="socials text-center text-md-right pt-3">
                <ul>
                    <li><a href="http://facebook.com"><span class="sr-only">Facebook</span></a></li>
                    <li><a href="http://twitter.com"><span class="sr-only">Twitter</span></a></li>
                    <li><a href="http://instagram.com"><span class="sr-only">Instagram</span></a></li>
                    <li><a href="http://pinterest.com"><span class="sr-only">Pinterest</span></a></li>
                    <li><a href="http://youtube.com"><span class="sr-only">YouTube</span></a></li>
                </ul>
            </nav>
        </div>
    </div> <!-- Justify-Content-Between -->
</header>