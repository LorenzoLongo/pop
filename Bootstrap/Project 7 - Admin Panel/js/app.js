(function() {
    'use strict';
    window.addEventListener('load', function() {

        fetch('data.json').then(data => data.json()).then(response => {
            new Morris.Line({
                element: 'sales',
                data: response,
                xkey: 'year',
                ykeys: ['value'],
                labels: ['Value']
            })
        });


        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();