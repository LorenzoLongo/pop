# Bootstrap Course 

This folder contains all the files concerning the Udemy Bootstrap course. This includes applications, assignments and projects. Also a theoretical summary will be provided.

> LINK: https://www.udemy.com/learn-bootstrap-4-the-most-popular-html5-css3-js-framework/

## Bootstrap Course Certificate

Certificate of completion of the Bootstrap Course

![Certificate of completion Bootstrap 4](certificate_bootstrap.jpg)

## Course Project Results

### Project 1: Carolina Spa
![Carolina Spa](CarolinaSpa.mp4)

### Project 2: Furniture Store
![Furniture Store](FurnitureStore.mp4)

### Project 3: RealState
![RealState](RealState.mp4)

### Project 4: Music Festival
![Music Festival](MusicFestival.mp4)

### Project 5: Architecture Contruction
![Architecture Contruction](ArchitectureConstruction.mp4)

### Project 6: Fiverr Clone
![Fiverr Clone](FiverrClone.mp4)

### Project 7: Admin/Login
![Admin/Login](Admin.mp4)

### Project 8: Culinary School
![Culinary School](CulinarySchool.mp4)

## Bootstrap Logbook

    Lorenzo Longo - 26/04/2019
        > Bootstrap Course Introduction
        
    Lorenzo Longo - 27/04/2019
        > Watching Course Videos 1-12
            -> Grid
            -> Typography Classes
            -> Margins & Paddings
            -> Images in Bootstrap
            -> Tables in Bootstrap
            -> Buttons
            -> Card Component
        > Creating Section 2 Application
        
    Lorenzo Longo - 28/04/2019
        > Watching Course Videos 13-22
            -> Masonry Effect
            -> Carousel Component
            -> Accordion/Collapse Component
            -> Modal Component
            -> ScrollSpy Component
            -> Tooltip Component
            -> Dropdown Component
            -> Jumbotron Component
            -> Navigation Menus
        > Updating Section 2 Application
        
    Lorenzo Longo - 29/04/2019
        > Watching Course Videos 23-34
            -> Working with Header Class
            -> Styling Main Navigation
            -> Adding Google Fonts
            -> Adding/Styling Carousel
            -> Creating/Styling New Website Section
            -> Creating/Styling Image Links Section
            -> Adding CSS3 Transitions
        > Creating Section 3 Application 
        
    Lorenzo Longo - 30/04/2019 
        > Watching Course Videos 35-46
            -> Creating/Styling Business Hours Section
            -> Adding a Table
            -> Creating/Styling Products Section
            -> Creating/Styling Appointment Section
            -> Creating/Styling Footer
            -> Creating/Styling Social Network Menu
            -> Printing Social Icons
            -> Splitting Website into PHP files
        > Updating Section 3 Application 
        
    Lorenzo Longo - 01/05/2019
        > Watching Course Videos 47-59
            Section 4:
                -> About Us Page HTML
                -> Styling the Hero Image
                -> Adding Content
                -> Adding a Gallery
                -> Adding the Business Hours Table
            
            Section 5:
                -> Creating/Styling the Services Page
                -> Tab/Collapse Component
                -> Creating Coupon Sidebar

            Section 6:
                -> Creating the Products Page
                -> Creating the Single Product Page
                
            Section 7:
                -> Creating/Styling the Contact Form 
                
        > Updating Section 3 Application (Sections: 4-5-6-7)
        
    Lorenzo Longo - 03/05/2019
        > Watching Course Videos 60-75
            Section 8:
                -> Creating/populating Database
                -> Creating Connection to the DB
                -> Printing the Products from the DB
                -> Printing Product in Single Product Page
            
            Section 9: 
                -> Adding Validation with JQuery
                -> Printing feedback in Contact Form
                -> Adding AJAX
                -> Creating Send PHP file
                -> Testing Project
        > Updating Section 3 Application (Sections: 8-9)
        
    Lorenzo Longo - 04/05/2019
        > Watching Course Videos 76-94
             Section 10:
                 -> Creating/Styling Hamburger Menu
                 -> Fixed Menu when Scrolling
                         
             Section 11: 
                 -> Migrate to new Bootstrap Version
                             
             Section 12:
                 -> Installing Wordpress Locally
                 -> Creating Theme
                 -> Adding Main Stylesheet
                             
             Section 13:
                 -> Printing Menu in Header
                 -> Creating Pages
                 -> Adding Main Navigation
                 -> Printing Main Menu
                          
             Section 14:
                 -> Creating Footer File
                 -> Adding Widget for Footer
                 -> Adding JScript Files in Footer
    
        > Updating Section 3 Application (Sections: 10-11)
        > Creating Section 12 Wordpress Theme (Sections 12-13-14)
                
    Lorenzo Longo - 06/05/2019
        > Watching Course Videos 95-104
            Section 15:
                -> Adding Page Template (About Us)
                -> Adding a Gallery 
                -> Adding Business Hours/Sidebar
                -> Creating a Widget in the Sidebar
            
            Section 16: 
                -> Adding Page Template (Services)
                -> Printing the Information
                -> Adding Coupon Sidebar
                
        > Updating Section 12 Wordpress Theme (Sections: 15-16) 
                
    Lorenzo Longo - 08/05/2019
        > Watching Course Videos 105-109
            Section 17:
                -> Adding Page Templates (Products)
                -> Adding Products into Wordpress
                -> Shortcut to Display all Products
                -> Adding Single Product Page
            
            Section 18: 
                -> Displaying the Contact Form
                
        > Updating Section 12 Wordpress Theme (Sections: 17-18) 
        
    Lorenzo Longo - 09/05/2019
        > Watching Course Videos 110-138
            Section 19:
                -> Adding Carousel
                -> Querying the DB to Display the Posts in the Slider
                -> Printing the Slogan
                -> Printing Info in the Home Page
                -> Adding Dynamic Page Titles to Pages
            
            Section 20: 
                -> Styling the Sitename
                -> Adding/Styling the Main Navigation
                -> Adding/Styling the Categories Section
                -> Adding/Styling the About Us Section
                -> Adding/Styling the Products Section
                -> Adding/Styling the Footer
                
        > Updating Section 12 Wordpress Theme (Sections: 19) 
        > Creating Section 20 Website --> E-Commerce 
        
    Lorenzo Longo - 11/05/2019
        > Watching Course Videos 139-156
            -> Adding/Styling Main & Sub Menu
            -> Adding/Styling Carousel/Slider
            -> Adding/Styling Properties Section
            -> Adding/Styling Contact Us Section
            -> Adding/Styling Blog Section
            -> Adding/Styling Testimonials Slider
            -> Adding/Styling Footer
        > Creating Section 21 Website --> Real State Website 
        
    Lorenzo Longo - 12/05/2019
        > Watching Course Videos 157-174
            -> Adding/Styling Top Section
            -> Adding/Styling Event Name & Date
            -> Adding/Styling Lineup Section
            -> Adding/Styling Countdown (jQuery)
            -> Adding/Styling Price List Section
            -> Adding/Styling Main Navigation
            -> Adding ScrollSpy with Scroll Effect
        > Creating Section 22 Website --> Music Festival
        
    Lorenzo Longo - 13/05/2019
        > Watching Course Videos 175-205
            Section 23:
                -> Adding/Styling the Main Navigation
                -> Styling the Hero Image
                -> Adding/Styling About Us Section
                -> Adding/Styling Basic Model House Section
                -> Adding/Styling Premier Model House Section
                -> Adding/Styling Elite Model House Section
                -> Adding Gallery with Card Columns
                -> Adding/Styling Footer
                
            Section 24:
                -> Adding/Styling the Header
                -> Adding/Styling the Main Navigation
                -> Styling the Hero Image
                -> Adding/Styling Main Section
                -> Adding CSS Animations
                -> Adding/Styling PRO Section
                -> Adding/Styling First Steps Section
                -> Adding/Styling Testimonials Carousel
                -> Adding/Styling Fiverr Guides Section
                -> Adding/Styling 2nd Header
                -> CSS Animation when Scrolling
                -> Adding/Styling Footer
                
        > Creating Section 23 Website --> Architecture/Construction
        > Creating Section 24 Website --> Fivver Clone Homepage
        
    Lorenzo Longo - 14/05/2019
        > Watching Course Videos 206-229
            Section 25:
                -> Adding/Styling Login Page
                -> Adding/Styling Clients Page
                -> Adding/Styling Sidebar Menu
                -> Adding Content
                -> Validate Bootstrap Forms
                -> Adding a Graph Library
                
            Section 26:
                -> Adding/Styling Main Section
                -> Adding/Styling Take a Class with Us Section
                -> Adding a Graphic after Titles
                -> Adding/Styling Upcoming Courses Section
                -> Adding/Styling Degree Section
                -> Adding/Styling Footer
                
        > Creating Section 25 Website --> Admin Panel
        > Creating Section 26 Website --> Culinary School
        
## Course Sections

### Section 1 (14:44)

Course introduction

### Section 2 (01:43:55)

Bootstrap 101 - The Basics of Bootstrap Grid, Buttons, Typography & More
    
### Section 3 (02:03:45) 

PROJECT: Building the Home Page

### Section 4 (22:28)

PROJECT: Building the About Us Page

### Section 5 (20:50)

PROJECT: Working with the Services Section

### Section 6 (11:02)

PROJECT: Building the Shop / Products Section

### Section 7 (09:00)

PROJECT: Building the Contact Section

### Section 8 (35:21)

PROJECT: PHP / MySQL - Creating the Database to display the Products

### Section 9 (45:36)

PROJECT: Building the Contact Form with jQuery, AJAX and PHP

### Section 10 (18:42)

Adding a Responsive Navigation (Hamburger Menu)

### Section 11 (08:31)

How to Migrate your website into the latest version

### Section 12 (18:30)

Converting our Website into a WordPress Theme - First Steps

### Section 13 (33:15)

WordPress Theme: Working with the Header

### Section 14 (21:45)

WordPress Theme: Working with the Footer

### Section 15 (51:30)

WordPress Theme: Working with the About Us Page


### Section 16 (14:46)

WordPress Theme: Working with the Services Page

### Section 17 (24:09)

WordPress Theme: Working with the Products Page

### Section 18 (05:05)

WordPress Theme: Working with the Contact Us Page

### Section 19 (55:32)

WordPress Theme: Working with the Home Page

### Section 20 (58:24)

PROJECT 2 : Building an E-Commerce Website

### Section 21 (01:17:40)

PROJECT 3: Real State Website

### Section 22 (01:26:22)

PROJECT 4: Music Festival Website

### Section 23 (49:47)

PROJECT 5: Architecture / Construction Website

### Section 24 (01:22:16)

PROJECT 6: Building a Fivrr Clone Homepage

### Section 25 (46:25) 

PROJECT 7: Creating an Admin Panel

### Section 26 (43:00)

PROJECT 8: Website for Culinary School