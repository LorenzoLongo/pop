# Persoonlijk ontwikkelingsplan (POP) - opvolgingsdocument Academiejaar 2018-2019

Student:	Lorenzo Longo

Mentor:		Joris Maervoet



## Beschrijving - datum 15/02/2019 


Onderwerp:
 
Angular 7 en Bootstrap 4


Einddoelen (met evaluatiecriteria):

1. De student krijgt een grondig inzicht in alles wat Angular 7 betreft.
   * INDICATOR: De student behaalt een certificaat voor het volgen van een online cursus.
   * INDICATOR: De student creëert verschillende applicaties die in de cursus verwerkt zitten.

    > Cursus Angular 7 (vroeger Angular 2). In deze cursus is het de bedoeling om de architectuur van Angular te snappen en toe te passen. Qua toepassingen gaat het hier over single page applicaties en schaalbare, moderne web applicaties. 
    >
    > De cursus heeft 28 uur aan beeldmateriaal, 39 artikels en 95 resources. Het certificaat die zal behaald worden betreft een "Certificate of completion". 
    >
    > Deze cursus omvat volgende hoofdstukken (Angular basics, Debugging, Components + Databinding, Deep Dive Directives, Services + Dependicy injection, Routing [Changing pages], Observables, Forms in Angular, Pipes to transform output, Http requests, Authetication + route protection, Using Angular modules + Optimizing Apps, Deploy Angular apps, HttpClient, NgRx, Angular Universal, Animations, Adding offline capabilities with service workers, Basic Unit testing)
    >
    > Er zijn verschillende course project assignments aan de hoofdstukken verbonden, dit zullen de applicaties/projecten zijn die de student creëert.
    


2. De student krijgt een grondig inzicht in alles wat Bootstrap 4 betreft.
   * INDICATOR: De student behaalt een certificaat voor het volgen van een online cursus.
   * INDICATOR: De student creëert verschillende projecten die in de cursus verwerkt zitten.

    > Cursus Bootstrap 4. In deze cursus wordt bootstrap aangeleerd door het maken van 8 projecten. 
    >
    > De cursus heeft 18 uur aan beeldmateriaal en 21 resources. Het certificaat die zal behaald worden betreft een "Certificate of completion".
    >
    > Deze cursus omvat volgende hoofdstukken (Basics of Bootstrap, Project Wordpress website, Project E-commerce website, Project Real State website, Project Music Festival website, Project Architecture website, Project Fivvr Clone Homepage, Project creating an admin panel, Project Website for culinary school).
    >
    > Er zijn verschillende course project assignments aan de hoofdstukken verbonden, dit zullen de applicaties/projecten zijn die de student creëert.


3. De student integreert Angular en Bootstrap in eenzelfde applicatie.
   * INDICATOR: De student een proof of concept applicatie met Angular en Bootstrap.

    > Angular (basics, components + databinding, routing, authentication, forms).
    >
    > De webapplicatie zal omtrent films of voetbal zijn. 
    

Motivatie - betrokkenheid:

Web gerelateerde zaken interesseren mij het meest en liggen mij ook tevens het best doorheen de jaren in de opleiding. Angular en Bootstrap zouden daar voor mij een mooie uitbreiding op zijn, daar ik al een tijdje van plan was om me in beide te verdiepen. Dit nu samen combineren met POP zou ideaal zijn.

Motivatie - grenzen verleggen:

Met beide onderwerpen heb ik nagenoeg amper tot geen ervaring. Dus is het vanzelfsprekend dat ik hierin grote kennis bij kan vergaren en tegelijkertijd ook mijn grenzen zal verleggen.

Motivatie - professioneel profiel:

Het zou mooi zijn om beide competenties te kunnen toevoegen aan mijn CV.

Geef hier de opsomming van de kerndoelen van de opleiding waarbinnen je voorstel past: 

kerndoelen: 1.b, 3.a, 3.b, 5.c, 6.e, 8.i, 8.j, 8.k, 10.m, 10.n


Aan de hand van twee online cursussen op Udemy. Eén voor Angular 7 en Bootstrap 4. Hierbij wordt voor beide een certificaat behaald. Tevens wordt er gebruik gemaakt van andere online en offline bronnen. 
Verder worden voor beide ook voorbeelapplicaties gemaakt die aan bod komen in de online tutorials. Ten laatste wordt er een proof of concept applicatie gebouwd waarin Bootstrap en Angular samen gecombineerd worden.


> **Feedback door mentor -- 21/02/2019**
> 
> Dag Lorenzo, je staat al een eindje met je projectbeschrijving maar zal nog wat
> moeten uitzoeken. 
> Kan je aanvullen bij je einddoelen
> * over welke cursussen/certificaten het gaat ?
> * over wat voor soort applicatie het gaat?
> * kan je tussen haakjes bij het woord Angular in doelstelling 3 aangeven welke
>   deelaspecten van Angular ongeveer allemaal in de demo zullen verwerkt zitten?
>
> Veel succes!


> **Goedkeuring door mentor -- 25/02/2019**
> 
> Goedgekeurd!
> ps stuur gerust je manier van werken bij indien je aanvoelt dat je op een de andere manier sneller kan leren


## Planning - 25/02/2019


<Hou hier je werkplanning bij, op te stellen nadat je voorstel is goedgekeurd>

### Semester  2 
#### Week 1 - 2 (11/02/2019 - 22/02/2019)

     1) Indienen voorstel POP
     2) Wachten op goedkeuring
     3) Aanpassen voorstel POP
     
#### Week 3 (25/02/2019 - 01/03/2019)

>ANGULAR 7 CURSUS

    1) Wachten op officiele goedkeuring van voorstel
    2) Planning opstellen van het opleidingsonderdeel over het gehele semester
    3) Kopen van de online cursus op Udemy.com
    4) Starten met sectie 1 van de Angular 7 online cursus
    
#### Week 4 (04/03/2019 - 08/03/2019)

>KROKUSVAKANTIE

    (Geen planning)

#### Week 5 (11/03/2019 - 15/03/2019)

>ANGULAR 7 CURSUS

    1) Sectie 1: Getting started with Angular 7
    2) Sectie 2: Angular Basics
    3) Sectie 3: Course Project - Angular Basics
    4) Sectie 4: Debugging in Angular
    5) Sectie 5: Components & Deep Dive Databinding
    6) Sectie 6: Course Project - Components & Deep Dive Databinding
    7) Sectie 7: Deep Dive Directives
    8) Sectie 8: Course Project - Directives

#### Week 6 (18/03/2019 - 22/03/2019)

>ANGULAR 7 CURSUS

    1) Sectie 9: Using Services & Dependency Injection
    2) Sectie 10: Course Project - Services & Dependency Injection
    3) Sectie 11: Changing Pages with Routing
    4) Sectie 12: Course Project - Routing
    5) Sectie 13: Understanding Observables
    6) Sectie 14: Course Project - Obervables
    7) Sectie 15: Handling Forms in Angular Apps
    8) Sectie 16: Course Project - Forms
    
#### Week 7 (25/03/2019 - 29/03/2019)

>ANGULAR 7 CURSUS
 
    1) Sectie 17: Using Pipes to Transform Output
    2) Sectie 18: Making Http Requests
    3) Sectie 19: Course Project - Http
    4) Sectie 20: Authentication & Route Protection in Angular Apps
    5) Sectie 21: Using Angular Modules & Optimizing Apps
    6) Sectie 22: Deploying an Angular App
    7) Sectie 23: Bonus: The HttpClient
    8) Sectie 24: Bonus: Working with NgRx in our Project
    
#### Week 8 (01/04/2019 - 05/04/2019)

>ANGULAR 7 CURSUS
    
    1) Sectie 25: Bonus: Angular Universal
    2) Sectie 26: Angular Animations
    3) Sectie 27: Adding Offline Capabilities with Service Workes
    4) Sectie 28: A Basic Introduction to Unit Testing in Angular Apps
    5) Sectie 29: Angular Changes & New Features
    6) Sectie 30: Course Roundup
    7) Sectie 31: Custom Project & Workflow Setup
    8) Sectie 32: Bonus: TypeScript Introduction (for Angular 2 Usage)

#### Week 9 (08/04/2019 - 12/04/2019)

>PAASVAKANTIE

    (onder voorbehoud - om in te halen of verder te doen)

#### Week 10 (15/04/2019 - 19/04/2019)

>PAASVAKANTIE

    (onder voorbehoud - om in te halen of verder te doen)

#### Week 11 (22/04/2019 - 26/04/2019)

>BOOTSTRAP 4 CURSUS

    1) Sectie 1: Course Introduction
    2) Sectie 2: Bootstrap 101 - The Basics of Bootstrap Grid, Buttons, Typography & More
    3) Sectie 3: PROJECT: Building the Home Page
    4) Sectie 4: PROJECT: Building the About Us Page
    5) Sectie 5: PROJECT: Working with the Services Section
    6) Sectie 6: PROJECT: Building the Shop / Products Section
    7) Sectie 7: PROJECT: Building the Contact Section
    8) Sectie 8: PROJECT: PHP / MySQL - Creating the Database to display the Products

#### Week 12 (29/04/2019 - 03/05/2019)

>BOOTSTRAP 4 CURSUS

    1) Sectie 9: PROJECT: Building the Contact Form with jQuery, AJAX and PHP
    2) Sectie 10: Adding a Responsive Navigation (Hamburger Menu)
    3) Sectie 11: How to Migrate your website into the latest version
    4) Sectie 12: Converting our Website into a WordPress Theme - First Steps
    5) Sectie 13: WordPress Theme: Working with the Header
    6) Sectie 14: WordPress Theme: Working with the Footer
    7) Sectie 15: WordPress Theme: Working with the About Us Page
    8) Sectie 16: WordPress Theme: Working with the Services Page

#### Week 13 (06/05/2019 - 10/05/2019)

>BOOTSTRAP 4 CURSUS

    1) Sectie 17: WordPress Theme: Working with the Products Page
    2) Sectie 18: WordPress Theme: Working with the Contact Us Page
    3) Sectie 19: WordPress Theme: Working with the Home Page
    4) Sectie 20: PROJECT 2 : Building an E-Commerce Website
    5) Sectie 21: PROJECT 3: Real State Website
    6) Sectie 22: PROJECT 4: Music Festival Website
    7) Sectie 23: PROJECT 5: Architecture / Construction Website
    8) Sectie 24: PROJECT 6: Building a Fivrr Clone Homepage

#### Week 14 (13/05/2019 - 17/05/2019)

>BOOTSTRAP 4 CURSUS

    1) Sectie 25: PROJECT 7: Creating an Admin Panel
    2) Sectie 26: PROJECT 8: Website for Culinary School
        
>ANGULAR + BOOTSTRAP PROJECT
    
    1) Start building the application

#### Week 15 (20/05/2019 - 25/05/2019)

>ANGULAR + BOOTSTRAP PROJECT

    1) Building the application

#### Week 16 (27/05/2019 - 01/06/2019)

>ANGULAR + BOOTSTRAP PROJECT

    1) Finishing the application 

## Logboek

> LOGBOEKEN ZULLEN AANWEZIG ZIJN BINNENIN DE VERSCHILLENDE SECTIES

<Noteer hier je activiteiten, gevonden informatie, behaalde milestones, 
belangrijke gebeurtenissen, problemen, oplossingen...
Zet voor elke nieuwe entry op een aparte lijn je naam en de datum.
Als je dat wil, mag je de zuiver technische informatie in een aanvullend 'technisch logboek'
bijhouden (binnen je repository). In dat geval kan je hier bij de verschillende entries verwijzen
naar dat technisch logboek.>

### ANGULAR 7 CURSUS 
#### SECTIE 1 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%201

#### SECTIE 2 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%202

#### SECTIE 3 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%203

#### SECTIE 4 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%204

#### SECTIE 5 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%205

#### SECTIE 6 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%206

#### SECTIE 7 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%207

#### SECTIE 8 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%208

#### SECTIE 9 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%209

#### SECTIE 10 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2010

#### SECTIE 11 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2011

#### SECTIE 12 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2012

#### SECTIE 13 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2013

#### SECTIE 14 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2014

#### SECTIE 15 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2015

#### SECTIE 16 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2016

#### SECTIE 17 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2017

#### SECTIE 18 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2018

#### SECTIE 19 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2019

#### SECTIE 20 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2020

#### SECTIE 21 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2021

#### SECTIE 22 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2022

#### SECTIE 23 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2023

#### SECTIE 24 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2024

#### SECTIE 25 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2025

#### SECTIE 26 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2026

#### SECTIE 27 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2027

#### SECTIE 28 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2028

#### SECTIE 29 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2029

#### SECTIE 30 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2030

#### SECTIE 31 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2031

#### SECTIE 32 - LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Section%2032


### BOOTSTRAP CURSUS 
#### LOGBOEK

>LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Bootstrap

## Eerste tussenrapportering - (29/03)


Stand van zaken:

<Geef hier een korte stand van zaken van wat er gebeurd is sinds de start: 
Wat is er gerealiseerd? Wat moet nog gebeuren? Wat zijn bijzondere gebeurtenissen?
Wat zijn problemen die je nog moet oplossen?>

### Stand van zaken

    Momenteel ben ik nog volop bezig met het leren van de Angular online cursus. 
        Gedaan:
            De basis
                > componenten creëren
                > custom componenten gebruiken
                > component styles gebruiken
                > component templates gebruiken
                > Databinding 
                > String Interpolation
                > Property Binding
                > Event Binding
                > Bindable properties & events
                > Two-Way-Databinding
                > Directives (basis)
                > NgIf/Else gebruiken
                > Elmenten stylen met NgStyle
                > Dynamische CSS klassen instellen met NgClass
                > NgFor gebruiken + index ophalen
                    
            Bij dit onderdeel zijn enkele opdrachten en een project gemaakt.
            
            Debuggen:
                > Angular errors leren interpreteren en oplossen
                > Debuggen door middel van Sourcemaps (binnen de browser)
                > Augury extensie leren gebruiken binnenin een Angular applicatie
               
            Componenten & Deep Dive Databinding
                > Bestaande app in componenten opsplitsen
                > Custom property binding
                > Alias instellen voor een custom property 
                > Custom event binding
                > Alias instellen voor een custom event
                > View Encapsulation (theorie)
                > Local references in templates gebruiken
                > @ViewChild gebruiken om toegang te krijgen tot de template/DOM
                > Ng-Content gebruiken
                > Component lifecycle (theorie)
                > @ContentChild gebruiken om toegang te krijgen tot Ng-Content
                
            Bij dit onderdeel zijn enkele opdrachten en een project gemaakt.
            
            Deep Dive Directives: 
                > Attribute directive creëren 
                > Renderer klasse gebruiken
                > HostListener gebruiken om naar Host Events te luisteren
                > HostBinding om Host Properties te binden
                > Binding Directive Properties
                > Structural Directives (theorie)
                > Een Structural Directive bouwen
                > NgSwitch gebruiken
                
            Bij dit onderdeel is een project gemaakt.
            
            Services & Dependency Injection
                > Waarom Services gebruiken (theorie)
                > Logging Service creëren
                > Service injecten in componenten
                > Data service creëren
                > Hiërarchische Injector (theorie)
                > Services binnen services injecteren
                > Services voor Cross-Component Communicatie
                
            Bij dit onderdeel is een opdracht en een project gemaakt. 
            
        Wat moet nog gebeuren?:
            Nog een heleboel Angular secties moeten worden doorgenomen. Vervolgens zal de Bootstrap online cursus gestart worden en om af te sluiten nog het proof of concept project waarin beiden worden gecombineerd.
            
        Problemen?;
            Momenteel zijn er geen praktische problemen. Bij de start was erv vooral een probleem om alles foutloos op GIT te pushen. Dit is intussen opgelost en heb ik de methodiek te pakken gekregen om foutloos te pushen. 
            
        Vergelijking met de originele planning:
            Op dit moment zit ik enkele secties achter op de originele planning. Daar zou ik eind volgende week klaar zijn met de Angular cursus. Maar dit zal waarschijnlijk nog een week extra werk zijn tijdens de paasvakantie. 
                   
Reflectie:

<Hoe werkt je aanpak? Waar had je achteraf gezien beter anders te werk kunnen gaan?
Hoe ga je eventueel bijsturen? Welke aanpassingen zijn er nodig in je planning?>

### Reflectie

    Hoe werkt je aanpak?:
        De aanpak werkt goed voor het inoefenen en studeren van de leerstof, maar is enorm tijdrovend. Daar ik de meeste video's 2 tot 3 maal bekijk om:
            1) De stof al eens een eerste keertje te bekijken
            2) De theorie te noteren in een notitieboekje
            3) Deze in te oefenen in een applicatie/project
            
    Waar anders te werk gaan?: 
        Op zich is er niet zoveel mis met de methodiek om het te leren, alleen de efficiëntie moet de hoogte in. Het is dus vooral het gebrek aan zichtbare progressie wat het vervelendste is op dit moment. 
        
    Waar bijsturen + planning?:
        De planning wordt op dit moment met één week verschoven. Dit was reeds in de planning voorzien (zie paasvakantie).  

Zelfevaluatie:

<Geef jezelf hier een score (gaande van 'NVT' tot 'A') voor de items die in het 
evaluatiedocument een witte achtergrond hebben>

### Zelfevaluatie

    Nieuw verworven kennis: C
    
    Toepassing aangetoond: B
    
    Planning: B
    
    Initiatief - inzet: B
    
    Rapportering (opvolgingsdocument): C
        
    
Feedback vanwege mentor - 10/04/2019:

> Lorenzo, 
> 
> wat mij betreft zijn je logboek, planning en manier van rapporteren tot hiertoe zeker okee. 2 zaken om zeker op volgen:
>  - stuur je planning/ manier van werken gerust bij indien ze je teveel tegenhoudt om je doelstellingen te bereiken
>  - zorg ervoor dat je tegen het einde van de rit ook nog aan de toepassing werkt  (stuur je planning in die zin gerust ook bij)
>
> Akkoord met de zelfevaluatie, al is de toepassing nog niet aangetoond.
>
> Veel succes met het verdere verloop !



# Eindrapportering - 02/05/2019



## Eindrapportering:

<Geef hier een eindstand van wat je bereikt hebt (en wat niet).
Hierin verwijs je ook naar eventuele bijkomende documenten of informatie om dit te onderbouwen.>

### Angular Course

   * INDICATOR: De student behaalt een certificaat voor het volgen van een online cursus.
    
> Certificaat: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/blob/master/Angular/certificate.jpg
   
   * INDICATOR: De student creëert verschillende applicaties die in de cursus verwerkt zitten.
   
> De verschillende gemaakte applicaties en assignments zijn terug te vinden in de Angular map. Per sectie is er een beschrijving te vinden over het onderwerp met zijn sub-onderdelen en waar kon is ook een video van het eindresultaat terug te vinden. 

#### Wat niet bereikt in Angular Course

> Section 24 NgRx heeft een update gekregen tijdens het semester en heb deze content niet ingestudeerd. 


### Bootstrap Course

   * INDICATOR: De student behaalt een certificaat voor het volgen van een online cursus.
   
> Certificaat: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/blob/master/Bootstrap/certificate_bootstrap.jpg

   * INDICATOR: De student creëert verschillende projecten die in de cursus verwerkt zitten.
   
> De verschillende gemaakte websites zijn terug te vinden in de Bootstrap map. Er zijn ook video's van het eindresultaat terug te vinden in de volgende link: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Bootstrap

#### Wat niet bereikt in Bootstrap Course

> Alles van deze cursus is bereikt.

### Eigen Angular-Bootstrap Webapp

   * INDICATOR: De student een proof of concept applicatie met Angular en Bootstrap.
   
> Er is een volledig afgewerkte Angular-Bootstrap applicatie gemaakt. Met verschillende concepten en componenten uit beide frameworks in verwerkt. 

#### Wat niet bereikt in Eigen Angular-Bootstrap Webapp

> Daar het heel veel leerstof was binnen een toch vrij korte tijdspanne is het niet haalbaar om alle concepten en componenten toe te voegen aan dit project. Maar alles wat in het project verwerkt zit is wel boven de basis concepten. 
   
## Reflectie:

<Geef hier een reflectie over je aanpak, je originele einddoelen, en je resultaat: Wat was goed?
 Wat had beter gekund? Waren er alternatieven voor je aanpak? Wat zou je achteraf gezien anders
 gedaan hebben? Hoe zou je je nog verder kunnen profileren in de richting van je onderwerp? >

> POSITIEF: De einddoelen zijn zo goed als volledig behaald. Ondanks dat het toch vrij zware cursussen waren die heel praktijkgericht en tijdsinstensief waren. Doordat zowel leerstof praktisch werd aangetoond en er vervolgens ook opdrachten waren om zelf de leerstof toe te passen, was het makkelijk om de leerstof op te pikken. Ook positief was dat ik toch ruimte heb gelaten in de planning om eventuele achterstanden door het semester heen op te halen. Dit heeft ervoor gezorgd dat ik deze OPO zo goed als volledig heb kunnen afwerken (lees: indicators behalen).

> WAT HAD BETER GEKUND?: Er hadden nog meer voorbeelden van concepten en componenten van zowel Angular als Bootstrap in de eindapplicatie kunnen verwerkt worden (bv. ScrollSpy, collapsebar (Bootstrap), HttpClient extra voorbeelden, live preview van data die naar de firbebase DB zal worden gepusht (Angular)).
 
 > In het begin was het nogal moeilijk om aan de planning (lees: tempo) te voldoen, daar ik al snel wat achter op schema stond daardoor. Gelukkig had ik hier rekening mee gehouden in diezelfde planning.
 
 > Ook was het bij momenten vervelend om afhankelijk te zijn van een laptop met liefst een tweede scherm aan verbonden. Hierdoor zat ik meestal vast in dezelfde ruimte voor ettelijke uren. Dit was niet altijd even bevorderlijk voor de vooruitgang. 
 
 > WAREN ER ALTERNATIEVEN VOOR JE AANPAK?: Er zijn zeker genoeg alternatieven om zowel Angular als Bootstrap in te studeren. Bijvoorbeeld door middel van boeken te gaan studeren of workshops te gaan doen. Dus in dat opzicht kon er eventueel wat meer geswitched worden tussen deze drie media (online cursus, boek en workshop) om wat meer diversiteit in het studeertraject te verkrijgen. 

> WAT ZOU JE ANDERS GEDAAN HEBBEN?: Zoals in de delen hierboven al vermeld zou ik wat vroeger gestart zijn en eventueel wat geswichted hebben van medium om alle indicatoren te behalen. 

> HOE VERDERE PROFILERING?: Met deze cursussen heb ik al reeds vele concepten van beide frameworks gezien en onder de knie gekregen. Maar in het zelf maken van de applicatie is toch gebleken dat ervaring toch ook een grote rol speelt. Veel was nog aftasten in het begin van het maken van de eindapplicatie met zowel Angular als Bootstrap, dus in dat opzicht zou het goed zijn om nog meer ervaring op te doen door meer projecten te maken. Daar bovenop is het nog mogelijk om bij beide frameworks nog over andere concepten bij te leren.  

## Inhoud Portfolio:

<Geef hier een overzicht van de informatie die je portfolio bevat> 

### Theorie

> Er is een PDF gemaakt met een groot deel van de geschreven theorie van de Angular cursus. 

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular/Theory

### Angular
    
> Alles binnenin de Angular map heeft te maken met de Angular cursus (32 secties). Bij sommige van de secties kunnen er video's gevonden worden van de eindresultaten van gemaakte applicaties binnenin die secties. Alsook (in zowel logboek als de ReadMe's van de "Applications" en "Assignments" folders) een beschrijving van welke sub-onderdelen die in deze sectie aan bod komen. 

> De links naar de logboeken staan in het onderdeel "Logboek" van dit document. De "Applications" en "Assignments" mappen zitten binnenin elke sectie waar het logboek zich bevindt. 

> Voorbeeld:

![Section Example](LogbookOfSection.jpg)

![Section Example Applications](ApplicationsOfSection.jpg)

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular

### Bootstrap 

> Alles binnenin de Bootrap map heeft te maken met de Bootstrap cursus. Deze heeft een onderverdeling waar per project een map bestaat. Deze cursus had 8 projecten + een sectie met basisoefeningen (map: Bootstrap 101). Project 1: Carolina Spa + Carolina Spa Wordpress Theme, Project 2: E-Commerce website, Project 3: RealState website, Project 4: Muziek Festival website, Project 5: Architecture-Construction website, Project 6: Fiverr Clone website, Project 7: Admin Panel, Project 8: Culinary School website.

> De eindresultaten van al deze websites hebben elk een video die zich in de onderstaande link bevinden: 

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Bootstrap

> OPMERKING: Ook het logboek voor de Bootstrap cursus is in de ReadMe van de bovenstaande link te vinden met daarbij uitleg met wat er per project is uigevoerd aan Bootstrap concepten.

> OPMERKING: De websites kunnen ook via de localhost (WAMP/MAMP) bekeken worden.

### Eindapplicatie

> Alles binnen de "Angular_Bootstrap_App" map bevat de eindapplicatie (code, readme, packages). In onderstaande link zijn er ook video's te vinden van het eindresultaat:

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/tree/master/Angular_Bootstrap_App

> De applicatie kan ook met het "ng serve" commando binnen de "kaagent" map opgestart worden en vevolgens bereikt worden met de url -> localhost:4200.

## Bronnenlijst:

> LINK: https://git.ikdoeict.be/lorenzo.longo/1819pop-lorenzolongo/blob/master/Bronnenlijst_POP.docx

## Zelfevaluatie:

<Geef jezelf hier een score (gaande van 'NVT' tot 'A') voor de items die in het 
evaluatiedocument een witte achtergrond hebben>

    Nieuw verworven kennis: B
    
    Toepassing aangetoond: B
    
    Planning: B
    
    Initiatief - inzet: B
    
    Rapportering (opvolgingsdocument): C








